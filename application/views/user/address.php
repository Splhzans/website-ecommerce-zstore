<!-- breadcrumb -->
<div class="container">
	<?php if( $this->session->flashdata('notification') ) : ?>
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	     	<strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> address!
	     	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	     	</button>
	    </div>
	 <?php endif; ?>

	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('profile'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Profile
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('setting'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Setting
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			Address
		</span>
	</div>
</div>

<!-- Product -->
<div class="bg0 m-t-30 p-b-140">
	<div class="container">
		<div class="row">
		<?php foreach( $address as $a ) : ?>
			<?php $city = $this->Rajaongkir_model->getCity($a['city'], $a['province']); ?>
			<div class="card w-50 col-6">
			  <div class="card-body">
			    <h5 class="card-title p-b-10"><?= $a['name']; ?></h5>
			    <p class="card-text"><?= $a['telephone']; ?></p>
			    <p class=""><?= $a['complete']; ?></p>
			    <?php if( $city['type'] == 'Kabupaten' ) : ?>
			    	<p class="card-text"><?= 'KAB. ' .strtoupper($city['city_name']); ?></p>
		    	<?php else : ?>
			    	<p class="card-text"><?= strtoupper($city['city_name']); ?></p>
		    	<?php endif; ?>
			    <p class="card-text"><?= strtoupper($city['province']); ?></p>
			    <p class="card-text">ID <?= $a['postal_code']; ?></p><br>
			    <a href="<?= base_url('address/edit/') .$a['id'] .'/' .$a['province']; ?>" class="card-link">edit</a>
    			<a href="<?= base_url('address/delete/') .$a['id']; ?>" class="card-link">delete</a>
			  </div>
			</div>
		<?php endforeach; ?>
		</div>

		<div class="flex-c-m flex-w w-full p-t-50">
			<a href="<?= base_url('address/add'); ?>">
				<button class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
					add address
				</button>
			</a>
		</div>
	</div>
</div>