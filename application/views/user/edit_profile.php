<!-- Content page -->
<section class="bg0 p-t-25 p-b-120">
	<div class="container">
		<form action="<?= base_url('editProfile/update'); ?>" method="post">
            <?= form_hidden('id', $user['id']); ?>
            <?= form_hidden('img', $user['picture']); ?>
            <?= form_hidden('email', $user['email']); ?>

            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-28 p-r-30 p-t-12" type="file" name="picture" value="<?= $user['picture']; ?>">
				<img src="<?= base_url(); ?>assets/img/<?= $user['picture']; ?>" class="how-pos4 pointer-none text-right rounded-circle pct-l" width="30px">
			</div>

			<div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-45 text-right" type="text" name="name" value="<?= $user['name']; ?>" required>
				<p class="how-pos4 pointer-none">Name</p>
			</div>

			<div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111  size-116 cl2 text-right" type="date" name="birthday" value="<?= $user['birthday']; ?>" required>
				<p class="how-pos4 pointer-none">Birthday</p>
			</div>

			<div class="bor8 how-pos4-parent border-bot cf">
				<select name="gender" class="gender cl2">
					<option value="male" <?= $user['gender'] == 'male' ? 'selected' : ''; ?>>Male</option>
					<option value="female" <?= $user['gender'] == 'female' ? 'selected' : ''; ?>>Female</option>
				</select>
				<p class="how-pos4 pointer-none">Gender</p>
			</div>

			<button type="Submit" name="insert" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 m-t-25 p-lr-15 trans-04 pointer">
				save
			</button>
		</form>
	</div>
</section>	