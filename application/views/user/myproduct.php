<!-- breadcrumb -->
<div class="container">
	<?php if( $this->session->flashdata('notification') ) : ?>
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	     	<strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> product!
	     	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	     	</button>
	    </div>
	 <?php endif; ?>

	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('profile'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Profile
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			My Product
		</span>
	</div>
</div>

<!-- Product -->
<div class="bg0 m-t-23 p-b-140">
	<div class="container">

		<?php if( !$product ) : ?>
			<div class="row">
				<h1 class="mx-auto text-muted m-t-70 m-b-25"><i>your product is empty</i></h1>
			</div>
			<div class="row">
				<button class="btn btn-success mx-auto m-b-90" data-toggle="modal" data-target="#modal-create">Add</button>
			</div>
		<?php else : ?>

		<div class="flex-w flex-sb-m p-b-52">
			<div class="flex-w flex-l-m filter-tope-group m-tb-10">
				<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
					All Products
				</button>
				<?php foreach( $category as $cat ) : ?>
					<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".<?= $cat['category']; ?>">
						<?= $cat['category']; ?>
					</button>
				<?php endforeach; ?>
			</div>
		</div>

		<div class="row isotope-grid">
			<?php foreach( $product as $prod ) : ?>
					<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item <?= $prod['category']; ?>">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-pic hov-img0">
								<img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" alt="IMG-PRODUCT" class="img-p">

								<a href="<?= base_url('myproduct/detail/'). $prod['id']; ?>" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
									View
								</a>
							</div>

							<div class="block2-txt flex-w flex-t p-t-14">
								<div class="block2-txt-child1 flex-col-l ">
									<a href="<?= base_url('myproduct/detail/'). $prod['id']; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
										<?= $prod['name']; ?>
									</a>

									<span class="stext-105 cl3">
										Rp <?= number_format($prod['price'], 0,',','.'); ?>
									</span>
								</div>

								<div class="block2-txt-child2 flex-r p-t-3">
									<button class="btn-addwish-b2 dis-block pos-relative" data-toggle="modal" data-target="#modal-update">
										<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-edit.png" alt="ICON">
									</button>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal Update -->
					<div class="modal fade m-t-75" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit Product</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <?= form_open_multipart('myproduct/update'); ?>
					      <?= form_hidden('id', $prod['id']); ?>
					      <?= form_hidden('seller', $this->session->userdata('id')); ?>
                          <?= form_hidden('img', $prod['picture']); ?>
				          <div class="modal-body">
				            <div class="row">
				              <input type="hidden" name="seller" value="<?= $this->session->userdata('id'); ?>">
				              <div class="form-group col-6">
				                <label for="picture">Picture (1200x1485)</label>
				                <input type="file" class="form-control-file" id="picture" name="picture">
				              </div>
				              <div class="form-group col-6">
				                <label for="name">Name</label>
				                <input type="text" name="name" class="form-control" id="name" value="<?= $prod['name']; ?>" required>
				              </div>

				              <div class="form-group col-6">
				                <label for="price">Price</label>
				                <input type="text" name="price" class="form-control" id="price" placeholder="0" value="<?= $prod['price']; ?>" required>
				              </div>

				              <div class="form-group col-6">
				                <label>Category</label>
				                <select class="form-control" name="category" required>
				                  <?php foreach( $category as $cat ) : ?>
				                  	<?php if( $cat['category'] == $prod['category'] ) : ?>
				                    	<option value="<?= $cat['category']; ?>" selected><?= $cat['category']; ?></option>
			                  		<?php else : ?>
				                    	<option value="<?= $cat['category']; ?>"><?= $cat['category']; ?></option>
			                  		<?php endif; ?>
				                  <?php endforeach; ?>
				                </select>
				              </div>

				              <div class="form-group col-6">
				                <label for="stock">Stock</label>
				                <input type="text" name="stock" class="form-control" id="stock" placeholder="0" value="<?= $prod['stock']; ?>" required>
				              </div>

				              <div class="form-group col-12">
				                <label for="description">Description</label>
				                <textarea name="description" class="form-control" rows="3" id="description" required><?= $prod['description']; ?></textarea>
				              </div>
				            </div>
				          </div>
				          <div class="modal-footer justify-content-between">
				            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				            <button type="submit" class="btn btn-primary">Save</button>
				          </div>
				          <?= form_close(); ?>
					    </div>
					  </div>
					</div>
			<?php endforeach; ?>
		</div>
		<div class="flex-c-m flex-w w-full p-t-10">
			<button class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04" data-toggle="modal" data-target="#modal-create">
				add product
			</button>
		</div>
	<?php endif; ?>
	</div>

	<!-- Modal -->
	<div class="modal fade m-t-75" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalScrollableTitle">Add Product</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <?= form_open_multipart('myproduct/create'); ?>
          <div class="modal-body">
            <div class="row">
              <input type="hidden" name="seller" value="<?= $this->session->userdata('id'); ?>">
              <div class="form-group col-6">
                <label for="picture">Picture (1200x1485)</label>
                <input type="file" class="form-control-file" id="picture" name="picture" required>
              </div>
              <div class="form-group col-6">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" required>
              </div>

              <div class="form-group col-6">
                <label for="price">Price</label>
                <input type="text" name="price" class="form-control" id="price" placeholder="0" required>
              </div>

              <div class="form-group col-6">
                <label>Category</label>
                <select class="form-control" name="category" required>
                  <?php foreach( $category as $cat ) : ?>
                    <option value="<?= $cat['category']; ?>"><?= $cat['category']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group col-6">
                <label for="stock">Stock</label>
                <input type="text" name="stock" class="form-control" id="stock" placeholder="0" required>
              </div>

              <div class="form-group col-12">
                <label for="description">Description</label>
                <textarea name="description" class="form-control" rows="3" id="description" required></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
          <?= form_close(); ?>
	    </div>
	  </div>
	</div>
</div>
