
<div class="container">
	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(''); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('profile'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Profile
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('myproduct'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			My Product
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			<?= $prod['name']; ?>
		</span>
	</div>
</div>


<!-- Product Detail -->
<section class="sec-product-detail bg0 p-t-65 p-b-60">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-7 p-b-30">
				<div class="p-l-25 p-r-30 p-lr-0-lg">
					<div class="wrap-slick3 flex-sb flex-w">
						<div class="wrap-slick3-dots"></div>
						<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

						<div class="slick3 gallery-lb">
							<div class="item-slick3" data-thumb="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>">
								<div class="wrap-pic-w pos-relative">
									<img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" alt="IMG-PRODUCT">

									<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>">
										<i class="fa fa-expand"></i>
									</a>
								</div>
							</div>

							<div class="item-slick3" data-thumb="<?= base_url(); ?>assets/img/product-detail2.jpg">
								<div class="wrap-pic-w pos-relative">
									<img src="<?= base_url(); ?>assets/img/product-detail2.jpg" alt="IMG-PRODUCT">

									<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?= base_url(); ?>assets/img/product-detail2.jpg">
										<i class="fa fa-expand"></i>
									</a>
								</div>
							</div>

							<div class="item-slick3" data-thumb="<?= base_url(); ?>assets/img/product-detail3.jpg">
								<div class="wrap-pic-w pos-relative">
									<img src="<?= base_url(); ?>assets/img/product-detail3.jpg" alt="IMG-PRODUCT">

									<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?= base_url(); ?>assets/img/product-detail3.jpg">
										<i class="fa fa-expand"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-5 p-b-30">
				<div class="p-r-50 p-t-5 p-lr-0-lg">
					<h4 class="mtext-105 cl2 js-name-detail p-b-14">
						<?= $prod['name']; ?>
					</h4>

					<span class="mtext-106 cl2">
						Rp <?= number_format($prod['price'], 0,',','.'); ?>
					</span>

					<p class="stext-102 cl3 p-t-23">
						<?= $prod['description']; ?>
					</p>

					<!--  -->
					<div class="flex-w flex-m p-l-100 p-t-40 respon7">
						<div class="flex-m bor9 p-r-10 m-r-11">
							<button class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 tooltip100" data-tooltip="Edit" data-toggle="modal" data-target="#modal-update">
								<i class="zmdi zmdi-edit"></i>
							</button>
						</div>
						<a href="<?= base_url('myproduct/delete/'). $prod['id']; ?>" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Delete" onclick="return confirm('Sure?');">
							<i class="fa fa-trash"></i>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="bor10 m-t-50 p-t-43 p-b-40">
			<!-- Tab01 -->
			<div class="tab01">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item p-b-10">
						<a class="nav-link active" data-toggle="tab" href="#description" role="tab">Description</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content p-t-43">
					<!-- - -->
					<div class="tab-pane fade show active" id="description" role="tabpanel">
						<div class="how-pos2 p-lr-15-md">
							<p class="stext-102 cl6">
								<?= $prod['description']; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $user = $this->User_model->get($prod['seller_id']); ?>
	<div class="bg6 flex-c-m flex-w size-302 m-t-73 p-tb-15">
		<span class="stext-107 cl6 p-lr-25">
			Seller: <?= $user['name']; ?>
		</span>

		<span class="stext-107 cl6 p-lr-25">
			Categories: <?= $prod['category']; ?>
		</span>
	</div>
</section>

<!-- Modal Update -->
<div class="modal fade m-t-75" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= form_open_multipart('myproduct/update'); ?>
      <?= form_hidden('id', $prod['id']); ?>
      <?= form_hidden('seller', $this->session->userdata('id')); ?>
      <?= form_hidden('img', $prod['picture']); ?>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" name="seller" value="<?= $this->session->userdata('id'); ?>">
          <div class="form-group col-6">
            <label for="picture">Picture (1200x1485)</label>
            <input type="file" class="form-control-file" id="picture" name="picture">
          </div>
          <div class="form-group col-6">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name" value="<?= $prod['name']; ?>" required>
          </div>

          <div class="form-group col-6">
            <label for="price">Price</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="0" value="<?= $prod['price']; ?>" required>
          </div>

          <div class="form-group col-6">
            <label>Category</label>
            <select class="form-control" name="category" required>
              <?php foreach( $category as $cat ) : ?>
              	<?php if( $cat['category'] == $prod['category'] ) : ?>
                	<option value="<?= $cat['category']; ?>" selected><?= $cat['category']; ?></option>
          		<?php else : ?>
                	<option value="<?= $cat['category']; ?>"><?= $cat['category']; ?></option>
          		<?php endif; ?>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="form-group col-6">
            <label for="stock">Stock</label>
            <input type="text" name="stock" class="form-control" id="stock" placeholder="0" value="<?= $prod['stock']; ?>" required>
          </div>

          <div class="form-group col-12">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" rows="3" id="description" required><?= $prod['description']; ?></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>