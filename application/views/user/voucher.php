<!-- breadcrumb -->
<div class="container">
	<?php if( $this->session->flashdata('notification') ) : ?>
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	     	<strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> product!
	     	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	     	</button>
	    </div>
	 <?php endif; ?>

	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg m-b-250 ">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('profile'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Profile
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			Voucher
		</span>
	</div>
</div>