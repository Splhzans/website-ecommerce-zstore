<!-- breadcrumb -->
<div class="container">
	<?php if( $this->session->flashdata('notification') ) : ?>
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	     	<strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> address!
	     	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	     	</button>
	    </div>
	 <?php endif; ?>

	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('features'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Features
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			Checkout
		</span>
	</div>

	<form method="post" action="<?= base_url('features/payment'); ?>">
		<?= form_hidden('address', $address); ?>
		<?= form_hidden('shipping', $ongkir); ?>
		<?= form_hidden('total', $total); ?>
		<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50 m-t-30">
			<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
				<h4 class="mtext-109 cl2 p-b-30">
					Checkout
				</h4>

				<label>keterangan</label>
				<div class="bor8 bg0 m-b-22">
					<textarea class="stext-111 cl2 plh3 size-110 p-lr-28 p-tb-25" name="description"></textarea>
				</div>


				<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-50 bor10">
					<select class="js-select2" name="metode">
						<option>Select metode...</option>
						<option value="Indomart">Indomart</option>
						<option value="Alfamart">Alfamart</option>
						<option value="Alfamidi">Alfamidi</option>
					</select>
					<div class="dropDownSelect2"></div>
				</div>

				<div class="row">
					<p class="col-6">Product Total</p>
					<p class="col-6 align-right">Rp. <?= number_format($subtotal, '0',',','.'); ?></p>
				</div>

				<div class="row">
					<p class="col-6">Postal Fee</p>
					<p class="col-6 align-right">Rp. <?php $ongkir = explode(",", $ongkir); echo number_format($ongkir[3], '0',',','.'); ?></p>
				</div>

				<div class="flex-w flex-t bor12 p-b-30"></div>

				<div class="flex-w flex-t p-t-27 p-b-33">
					<div class="size-208">
						<span class="mtext-101 cl2">
							Total:
						</span>
					</div>

					<div class="size-209 p-t-1">
						<span class="mtext-110 cl2 align-right">
							Rp. <?= number_format($total, '0',',','.'); ?>
						</span>
					</div>
				</div>

				<button type="submit" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
					Proceed to Checkout
				</button>
			</div>
		</div>
	</form>
</div>

