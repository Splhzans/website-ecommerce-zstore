<!-- Alert Message -->
<?php if( $this->session->flashdata('notification') ) : ?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Successfully</strong> edit <?= $this->session->flashdata('notification'); ?>!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php endif; ?>

<!-- Title3page -->
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?= base_url(); ?>assets/user/images/bg-07.jpg');">
	<h2 class="ltext-105 cl0 txt-center">
		Setting
	</h2>
</section>		


<!-- Content page -->
<section class="bg0 p-t-25">
	<div class="container">
		<div class="container">
			<a href="<?= base_url('EditProfile'); ?>" class="cl2 hov-cl1">
				<h5 class="dib">Edit Profile</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('address'); ?>" class="cl2 hov-cl1">
				<h5 class="dib">Adress</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('email'); ?>" class="cl2 hov-cl1">
				<h5 class="dib">E-mail</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('password'); ?>" class="cl2 hov-cl1">
				<h5 class="dib">Password</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('auth/logout'); ?>" class="cl2 hov-cl1">
				<h5 class="dib m-b-18">Logout</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
			</a>
		</div>
	</div>
</section>	