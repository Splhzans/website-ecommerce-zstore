<!-- Title page -->
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?= base_url(); ?>assets/user/images/bg-12.jpg');">
	<h2 class="ltext-105 cl0 txt-center">
		About
	</h2>
</section>	


<!-- Content page -->
<section class="bg0 p-t-75 p-b-120">
	<div class="container">
		<div class="row p-b-148">
			<div class="col-md-7 col-lg-8">
				<div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md">
					<h3 class="mtext-111 cl2 p-b-16">
						Our Story
					</h3>

					<p class="stext-113 cl6 p-b-26">
						<strong>ZStore</strong> is one of the website online store biggest <strong>Indonesia</strong>. With a feature shopping easy and satisfying make the website is quickly developed into an online store best in <strong>Indonesia</strong>.
					</p>

					<p class="stext-113 cl6 p-b-26">
						This site first made at the end of the 2019 by students <strong>SMKN 1 Katapang</strong>. This experience  of less enjoyable obtained when shopping online foregrounded the vision <strong>ZStore</strong> to provide a selling-buy  online safe for everyone. That's spirit which is the reason employees <strong>ZStore</strong> in work. 
					</p>

					<p class="stext-113 cl6 p-b-26">
						By doing the transaction through ZStore, we play a role in creating environment online business safe.
					</p>
				</div>
			</div>

			<div class="col-11 col-md-5 col-lg-4 m-lr-auto">
				<div class="how-bor1 ">
					<div class="hov-img0">
						<img src="<?= base_url(); ?>assets/user/images/about-03.jpg" alt="IMG">
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="order-md-2 col-md-7 col-lg-8 p-b-30">
				<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
					<h3 class="mtext-111 cl2 p-b-16">
						Our Mission
					</h3>

					<p class="stext-113 cl6 p-b-26">
						Mission us that is makes the economy <strong>Indonesia</strong> more advanced with flaunting some the product artificial <strong>Indonesia</strong>. And we aims to improve the income countries <strong>Indonesia</strong> with absorb the workforce and reduce unemployment in <strong>Indonesia</strong>.
					</p>

					<p class="stext-113 cl6 p-b-26">
						Anyone can build effort alone. With the network marketing broad, business success is in sight. So that the products marketed have a wider network, we always prioritize the quality of goods to be sold as well as an attractive visual appearance. Because the visual and quality of the product is the main key. And secure payment guarantees are no less important to us. Not only that, we provide protection for products that are publicly marketed, one of which is <strong>Zurich Product Liability</strong> which can provide guarantees in terms of legal responsibility of the insured to third parties for damage to property caused by the nature, condition, or quality of the product produced.
					</p>
				</div>
			</div>

			<div class="order-md-1 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
				<div class="how-bor2">
					<div class="hov-img0">
						<img src="<?= base_url(); ?>assets/user/images/about-04.png" alt="IMG">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	
