<!-- Alert Message -->
<?php if( $this->session->flashdata('message') ) { echo $this->session->flashdata('message'); } ?>

<!-- Content page -->
<section class="bg0 p-t-25 p-b-170">
	<div class="container">
		<form action="<?= base_url('Password/update'); ?>" method="post">
            <?= form_hidden('id', $user['id']); ?>

            <div class="bor8 how-pos4-parent border-bot p-b40">
				<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-45 lt-s-8 text-right" type="password" name="current" placeholder="********" required>
				<p class="how-pos4 pointer-none">Current Password</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-45 lt-s-8 text-right" type="password" name="password" placeholder="********" required>
				<p class="how-pos4 pointer-none">New Password</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-45 lt-s-8 text-right" type="password" name="repeat" placeholder="********" required>
				<p class="how-pos4 pointer-none">Repeat Password</p>
			</div>

			<button type="Submit" name="insert" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 m-t-25 p-lr-15 trans-04 pointer">
				save
			</button>
		</form>
	</div>
</section>	