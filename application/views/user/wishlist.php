<!-- breadcrumb -->
<div class="container">
	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			wishlist
		</span>
	</div>
</div>

<!-- Product -->
<div class="bg0 m-t-23 p-b-140">
	<div class="container">

		<?php if( !$wishlist ) : ?>
			<div class="row">
				<h1 class="mx-auto text-muted m-t-70 m-b-25"><i>your wishlist is empty</i></h1>
			</div>
			<div class="row">
				<a href="<?= base_url('shop'); ?>" class="btn btn-success mx-auto m-b-90">Shop Now</a>
			</div>
		<?php else : ?>

		<div class="flex-w flex-sb-m p-b-52">
			<div class="flex-w flex-l-m filter-tope-group m-tb-10">
				<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
					All Products
				</button>
				<?php foreach( $category as $cat ) : ?>
					<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".<?= $cat['category']; ?>">
						<?= $cat['category']; ?>
					</button>
				<?php endforeach; ?>
			</div>
		</div>

		<div class="row isotope-grid">
			<?php foreach( $wishlist as $wish ) : ?>
				<?php if( $wish['user_id'] == $this->session->userdata('id') ) : ?>
					<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item <?= $wish['category']; ?>">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-pic hov-img0">
								<img src="<?= base_url(); ?>assets/img/<?= $wish['picture']; ?>" alt="IMG-PRODUCT">

								<a href="<?= base_url('shop/detail/'). $wish['id']; ?>" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
									View
								</a>
							</div>

							<div class="block2-txt flex-w flex-t p-t-14">
								<div class="block2-txt-child1 flex-col-l ">
									<a href="<?= base_url('shop/detail/'). $wish['id']; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
										<?= $wish['name']; ?>
									</a>

									<span class="stext-105 cl3">
										Rp <?= number_format($wish['price'], 0,',','.'); ?>
									</span>
								</div>

								<div class="block2-txt-child2 flex-r p-t-3">
									<?php if( !$this->session->userdata('id') ) : ?>
										<button class="btn-addwish-b2 dis-block pos-relative" onclick="alert('you have to login');">
											<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
										</button>
									<?php else : ?>
										<?php $data = $this->db->get_where('t_wishlist', ['user_id' => $this->session->userdata('id'), 'product_id' => $wish['id'] ])->row_array(); ?>
										<?php if( $data ) : ?>
											<a href="<?= base_url('user/deleteWishlist/').$this->session->userdata('id').'/'.$wish['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
												<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-02.png" alt="ICON">
											</a>
										<?php else : ?>
											<a href="<?= base_url('user/addToWishlist/').$this->session->userdata('id').'/'.$wish['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
												<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
											</a>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	</div>
</div>
