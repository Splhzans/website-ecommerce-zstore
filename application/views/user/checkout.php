<!-- breadcrumb -->
<div class="container">
	<?php if( $this->session->flashdata('notification') ) : ?>
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	     	<strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> address!
	     	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	     	</button>
	    </div>
	 <?php endif; ?>

	<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
		<a href="<?= base_url(); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Home
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url('features'); ?>" class="stext-109 cl8 hov-cl1 trans-04">
			Features
			<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
		</a>

		<span class="stext-109 cl4">
			Checkout
		</span>
	</div>

	<form method="post" action="<?= base_url('features/metode'); ?>">
		<?= form_hidden('subtotal', $total); ?>
		<?php if( $this->uri->segment(3) ) : ?>
			<?= form_hidden('address', $this->uri->segment(3)); ?>
		<?php endif; ?>
		<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50 m-t-30">
			<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
				<h4 class="mtext-109 cl2 p-b-30">
					Checkout
				</h4>

				<div class="flex-w flex-t bor12 p-b-13">
					<div class="size-208">
						<span class="stext-110 cl2">
							Subtotal:
						</span>
					</div>

					<div class="size-209">
						<span class="mtext-110 cl2">
							Rp. <?= number_format($total, '0',',','.'); ?>
						</span>
					</div>
				</div>

				<div class="flex-w flex-t bor12 p-t-15 p-b-30">
					<div class="size-208 w-full-ssm">
						<span class="stext-110 cl2">
							Address:
						</span>
					</div>

					<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
						<p class="stext-111 cl6 p-t-2">
							There are no shipping methods available. Please double check your address, or contact us if you need any help.
						</p>
						
						<div class="p-t-15">
							<span class="stext-112 cl8">
								Calculate Shipping
							</span>

							<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
								<select class="js-select2" name="address"  onchange="document.location.href=this.options[this.selectedIndex].value;">
									<option>Select adress...</option>
									<?php foreach( $address as $a ) : ?>
										<?php if( $a['id'] == $this->uri->segment(3) ) : ?>
											<option value="<?= base_url('features/checkout/') .$a['id']; ?>" selected>
												<?php $city = $this->Rajaongkir_model->getCity($a['city'], $a['province']); ?>
												<p><?= $a['name']; ?> ~ </p>
												<p><?= $a['telephone']; ?> ~ </p>
												<p><?= $a['complete']; ?> ~ </p>
											    <?php if( $city['type'] == 'Kabupaten' ) : ?>
											    	<p><?= 'KAB. ' .strtoupper($city['city_name']); ?> ~ </p>
										    	<?php else : ?>
											    	<p><?= strtoupper($city['city_name']); ?> ~ </p>
										    	<?php endif; ?>
												<p><?= strtoupper($city['province']); ?> ~ </p>
												<p>ID <?= $a['postal_code']; ?></p>
											</option>
										<?php else : ?>
											<option value="<?= base_url('features/checkout/') .$a['id']; ?>">
												<?php $city = $this->Rajaongkir_model->getCity($a['city'], $a['province']); ?>
												<p><?= $a['name']; ?> ~ </p>
												<p><?= $a['telephone']; ?> ~ </p>
												<p><?= $a['complete']; ?> ~ </p>
											    <?php if( $city['type'] == 'Kabupaten' ) : ?>
											    	<p><?= 'KAB. ' .strtoupper($city['city_name']); ?> ~ </p>
										    	<?php else : ?>
											    	<p><?= strtoupper($city['city_name']); ?> ~ </p>
										    	<?php endif; ?>
												<p><?= strtoupper($city['province']); ?> ~ </p>
												<p>ID <?= $a['postal_code']; ?></p>
											</option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
								<div class="dropDownSelect2"></div>
							</div>

							<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
								<select class="js-select2" name="ongkir">
									<option>Select courier...</option>
									<?php foreach( $jne[0]['costs'] as $j ) : ?>
										<option value="JNE,<?= $j['service']; ?>,<?= $j['cost'][0]['etd']; ?>,<?= $j['cost'][0]['value']; ?>">JNE (<?= $j['service']; ?>) [ <?= $j['cost'][0]['etd']; ?> day ] => Rp. <?= $j['cost'][0]['value']; ?></option>
									<?php endforeach; ?>
									<?php foreach( $tiki[0]['costs'] as $t ) : ?>
										<option value="TIKI,<?= $t['service']; ?>,<?= $t['cost'][0]['etd']; ?>,<?= $t['cost'][0]['value']; ?>">TIKI (<?= $t['service']; ?>) [ <?= $t['cost'][0]['etd']; ?> day ] => Rp. <?= $t['cost'][0]['value']; ?></option>
									<?php endforeach; ?>
									<?php foreach( $pos[0]['costs'] as $p ) : ?>
										<option value="POS,<?= $p['service']; ?>,<?= $p['cost'][0]['etd']; ?>,<?= $p['cost'][0]['value']; ?>">POS (<?= $p['service']; ?>) [ <?= $p['cost'][0]['etd']; ?> ] => Rp. <?= $p['cost'][0]['value']; ?></option>
									<?php endforeach; ?>
								</select>

								<div class="dropDownSelect2"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="flex-w flex-t p-t-27 p-b-33">
					<div class="size-208">
						<span class="mtext-101 cl2">
							Total:
						</span>
					</div>

					<div class="size-209 p-t-1">
						<span class="mtext-110 cl2">
							Rp. <?= number_format($total, '0',',','.'); ?>
						</span>
					</div>
				</div>

				<button type="submit" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
					Proceed to Checkout
				</button>
			</div>
		</div>
	</form>
</div>