<!-- Product -->
<div class="bg0 m-t-23 p-b-140">
	<div class="container">
		<div class="flex-w flex-sb-m p-b-52">
			<div class="flex-w flex-l-m filter-tope-group m-tb-10">
				<a href="<?= base_url('shop'); ?>" class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 <?= !$this->uri->segment(3) ? 'how-active1' : ''; ?>" data-filter="*">
					All Products
				</a>
				<?php foreach( $category as $cat ) : ?>
					<a href="<?= base_url('shop/category/'). $cat['category']; ?>" class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 <?= $this->uri->segment(3) == $cat['category'] ? 'how-active1' : ''; ?>">
						<?= $cat['category']; ?>
					</a>
				<?php endforeach; ?>
			</div>
		</div>

		<div class="row isotope-grid">
			<?php foreach( $product as $prod ) : ?>
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item">
					<!-- Block2 -->
					<div class="block2">
						<div class="block2-pic hov-img0">
							<img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" alt="IMG-PRODUCT" class="img-p">

							<a href="<?= base_url('shop/detail/'). $prod['id']; ?>" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
								View
							</a>
						</div>

						<div class="block2-txt flex-w flex-t p-t-14">
							<div class="block2-txt-child1 flex-col-l ">
								<a href="<?= base_url('shop/detail/'). $prod['id']; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
									<?= $prod['name']; ?>
								</a>

								<span class="stext-105 cl3">
									Rp <?= number_format($prod['price'], 0,',','.'); ?>
								</span>
							</div>

							<div class="block2-txt-child2 flex-r p-t-3">
								<?php if( !$this->session->userdata('id') ) : ?>
									<a href="<?= base_url('auth/login'); ?>" class="btn-addwish-b2 dis-block pos-relative" onclick="return confirm('You have to login');">
										<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
									</a>
								<?php else : ?>
									<?php $data = $this->db->get_where('t_wishlist', ['user_id' => $this->session->userdata('id'), 'product_id' => $prod['id'] ])->row_array(); ?>
									<?php if( $data ) : ?>
										<a href="<?= base_url('wishlist/delete/').$this->session->userdata('id').'/'.$prod['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
											<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-02.png" alt="ICON">
										</a>
									<?php else : ?>
										<a href="<?= base_url('wishlist/create/').$this->session->userdata('id').'/'.$prod['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
											<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
										</a>
									<?php endif; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
