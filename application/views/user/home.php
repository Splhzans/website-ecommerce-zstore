<?php $this->Dashboard_model->AddVisitor(); ?>
<?php if( $this->session->userdata('id') && $this->session->flashdata('notif') ) : ?>
	<?php if( $this->cart->contents() ) : ?>
		<script>
			var confirm = confirm('You have several products in shopping cart before login. \n Want to add them to your account shopping cart?');
			if( confirm === true ) {
				<?php $this->session->set_flashdata('migrate', 'on'); ?>
				document.location.href = "<?= base_url('cart/create') ?>";
			}
		</script>
	<?php endif; ?>
<?php endif; ?>

<?php if( $this->session->flashdata('notif') ) : ?>
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<?= $this->session->flashdata('notif'); ?> <strong>successfully</strong>!
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
<?php endif; ?>

<!-- Slider -->
<section class="section-slide">
	<div class="wrap-slick1 rs1-slick1">
		<div class="slick1">
			<?php foreach( $banner as $ban ) : ?>
				<img src="<?= base_url(); ?>assets/img/<?= $ban['picture']; ?>" width="100%">
			<?php endforeach; ?>
		</div>
	</div>
</section>

<!-- Banner -->
<div class="sec-banner bg0">
	<div class="flex-w flex-c-m">
		<div class="size-202 m-lr-auto respon4">
			<!-- Block1 -->
			<div class="block1 wrap-pic-w">
				<img src="<?= base_url(); ?>assets/user/images/banner-04.jpg" alt="IMG-BANNER">

				<a href="<?= base_url('shop/category/women'); ?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
					<div class="block1-txt-child1 flex-col-l">
						<span class="block1-name ltext-102 trans-04 p-b-8">
							Women
						</span>

						<span class="block1-info stext-102 trans-04">
							Spring 2019
						</span>
					</div>

					<div class="block1-txt-child2 p-b-4 trans-05">
						<div class="block1-link stext-101 cl0 trans-09">
							Shop Now
						</div>
					</div>
				</a>
			</div>
		</div>

		<div class="size-202 m-lr-auto respon4">
			<!-- Block1 -->
			<div class="block1 wrap-pic-w">
				<img src="<?= base_url(); ?>assets/user/images/banner-05.jpg" alt="IMG-BANNER">

				<a href="<?= base_url('shop/category/men'); ?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
					<div class="block1-txt-child1 flex-col-l">
						<span class="block1-name ltext-102 trans-04 p-b-8">
							Men
						</span>

						<span class="block1-info stext-102 trans-04">
							Spring 2019
						</span>
					</div>

					<div class="block1-txt-child2 p-b-4 trans-05">
						<div class="block1-link stext-101 cl0 trans-09">
							Shop Now
						</div>
					</div>
				</a>
			</div>
		</div>

		<div class="size-202 m-lr-auto respon4">
			<!-- Block1 -->
			<div class="block1 wrap-pic-w">
				<img src="<?= base_url(); ?>assets/user/images/banner-06.jpg" alt="IMG-BANNER">

				<a href="<?= base_url('shop/category/bags'); ?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
					<div class="block1-txt-child1 flex-col-l">
						<span class="block1-name ltext-102 trans-04 p-b-8">
							Bags
						</span>

						<span class="block1-info stext-102 trans-04">
							New Trend
						</span>
					</div>

					<div class="block1-txt-child2 p-b-4 trans-05">
						<div class="block1-link stext-101 cl0 trans-09">
							Shop Now
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>


<!-- Product -->
<section class="sec-product bg0 p-t-100 p-b-50">
	<div class="container">
		<div class="p-b-32">
			<h3 class="ltext-105 cl5 txt-center respon1">
			Store Overview
			</h3>
		</div>
		<!-- Tab01 -->
		<div class="tab01">
			<!-- Tab panes -->
			<div class="tab-content">
				<!-- - -->
				<div class="tab-pane fade show active" id="best-seller" role="tabpanel">
					<!-- Slide2 -->
					<div class="wrap-slick2">
						<div class="slick2">
							<?php foreach( $product as $prod ) : ?>
							<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
								<!-- Block2 -->
								<div class="block2">
									<div class="block2-pic hov-img0">
										<img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" alt="IMG-PRODUCT" class="img-p">
										<a href="<?= base_url('shop/detail/'). $prod['id']; ?>" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
											View
										</a>
									</div>
									<div class="block2-txt flex-w flex-t p-t-14">
										<div class="block2-txt-child1 flex-col-l ">
											<a href="<?= base_url('shop/detail/'). $prod['id']; ?>" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
												<?= $prod['name']; ?>
											</a>
											<span class="stext-105 cl3">
												Rp <?= number_format($prod['price'], 0,',','.'); ?>
											</span>
										</div>
										<div class="block2-txt-child2 flex-r p-t-3">
											<?php if( !$this->session->userdata('id') ) : ?>
												<a href="<?= base_url('auth/login'); ?>" class="btn-addwish-b2 dis-block pos-relative" onclick="return confirm('You have to login');">
													<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
												</a>
											<?php else : ?>
												<?php $data = $this->db->get_where('t_wishlist', ['user_id' => $this->session->userdata('id'), 'product_id' => $prod['id'] ])->row_array(); ?>
												<?php if( $data ) : ?>
													<a href="<?= base_url('wishlist/delete/').$this->session->userdata('id').'/'.$prod['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
														<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-02.png" alt="ICON">
													</a>
												<?php else : ?>
													<a href="<?= base_url('wishlist/create/').$this->session->userdata('id').'/'.$prod['id']; ?>" class="btn-addwish-b2 dis-block pos-relative">
														<img class="icon-heart1 dis-block trans-04" src="<?= base_url(); ?>assets/user/images/icons/icon-heart-01.png" alt="ICON">
													</a>
												<?php endif; ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- NEWS -->
<section class="sec-blog bg0 p-t-60 p-b-90">
	<div class="container">
		<div class="p-b-66">
			<h3 class="ltext-105 cl5 txt-center respon1">
			News
			</h3>
		</div>
		<div class="row">
			<?php foreach( $news as $row ) : ?>
			<!-- news here -->
			<div class="col-sm-6 col-md-4 p-b-40">
				<div class="blog-item">
					<div class="hov-img0">
						<a href="<?= base_url('news/detail/'). $row['id']; ?>">
							<img src="<?= base_url(); ?>assets/img/<?= $row['picture']; ?>" alt="IMG-BLOG">
						</a>
					</div>
					<div class="p-t-15">
						<div class="stext-107 flex-w p-b-14">
							<span class="m-r-3">
								<span class="cl4">
									By
								</span>
								<span class="cl5">
									<?= $row['by']; ?>
								</span>
							</span>
							<span>
								<span class="cl4">
									on
								</span>
								<span class="cl5">
									<?= $row['date']; ?>
								</span>
							</span>
						</div>
						<h4 class="p-b-12">
						<a href="<?= base_url('news/detail/'). $row['id']; ?>" class="mtext-101 cl2 hov-cl1 trans-04">
							<?= $row['title']; ?>
						</a>
						</h4>
						<p class="stext-108 cl6">
							<?= word_limiter($row['contents'], 25); ?>
						</p>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>