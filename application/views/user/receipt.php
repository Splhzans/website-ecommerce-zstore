<?php
	$time = explode(",", $transaction['date']);
?>

<div class="row m-t-15 m-b-75">
	<div class="card col-8 m-auto">
	  <div class="card-body">
	  	<h5 class="card-title p-t-19" style="display: inline-block;">Total</h5>
    	<p class="card-text p-t-19 float-right" style="display: inline-block;">Rp. <?= number_format($transaction['total'], '0',',','.'); ?></p>
    	<hr style="margin-top: -5px;">
	    <h5 class="card-title text-center m-t-40">payment code:</h5>
	    <h1 class="card-title text-center m-b-30"><b><?= $transaction['code']; ?></b></h1>
	    <div class="card alert alert-warning">
	  		<div class="card-body">
	    		<p class="card-text" style="display: inline-block;">Please complete Your payment in Indomaret before: </p>
	    		<p class="card-text float-right" style="display: inline-block;"><?= $time[1]; ?></p>
	    	</div>
	    </div>
	    <a href="<?= base_url(); ?>" class="card-text"><p class="text-center m-t-50">&larr;&nbsp; back to home</p></a>
	  </div>
	</div>
</div>

	