<!-- Content page -->
<section class="bg0 p-t-25 p-b-70">
	<div class="container">
		<form action="<?= base_url('address/create'); ?>" method="post">
            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-150 p-r-45 text-right" type="text" name="name" required>
				<p class="how-pos4 pointer-none">Name</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-150 p-r-30 text-right" type="number" name="telephone" required>
				<p class="how-pos4 pointer-none">Telephone</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot cf">
				<select name="province" class="custom-select select-a col-3" onchange="document.location.href=this.options[this.selectedIndex].value;">
					<option selected></option>
					<?php foreach( $province as $p ) : ?>
						<?php if( $check == $p['province_id'] ) : ?>
							<option value="<?= base_url('address/add/') .$p['province_id']; ?>" selected><?= strtoupper($p['province']); ?></option>
						<?php else : ?>
							<option value="<?= base_url('address/add/') .$p['province_id']; ?>"><?= strtoupper($p['province']); ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
				<p class="how-pos4 pointer-none">Province</p>
			</div>

			<?php if( $check != 0 ) : ?>
            <div class="bor8 how-pos4-parent border-bot cf">
				<select name="city" class="custom-select select-a col-3	">
					<option selected></option>
					<?php foreach( $city as $c ) : ?>
						<?php if( $c['type'] == 'Kabupaten' ) : ?>
							<option value="<?= $c['city_id']; ?>"><?= 'KAB. ' .strtoupper($c['city_name']); ?></option>
						<?php else : ?>
							<option value="<?= $c['city_id']; ?>"><?= strtoupper($c['city_name']); ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
				<p class="how-pos4 pointer-none">City/District</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot">
				<input class="stext-111 cl2 plh3 size-116 p-l-150 p-r-30 text-right" type="number" name="code" required>
				<p class="how-pos4 pointer-none">Postal Code</p>
			</div>

            <div class="bor8 how-pos4-parent border-bot">
				<textarea class="stext-111 cl2 plh3 size-110 p-l-150 p-r-45 p-tb-10 text-right" name="complete"></textarea>
				<p class="how-pos4 pointer-none">Complete Address</p>
			</div>
			<?php endif; ?>

			<button type="Submit" name="insert" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 m-t-25 p-lr-15 trans-04 pointer">
				save
			</button>
		</form>
	</div>
</section>	