<!-- Title page -->
<section class="bg-img1 p-lr-15 p-t-80 p-b-30" style="background-image: url('<?= base_url(); ?>assets/user/images/bg-12.jpg');">
	<div class="row">
		<img src="<?= base_url(); ?>assets/img/<?= $user['picture']; ?>" class="m-t-30 m-l-10 col-lg-1">
		<div class="row col-lg-11">
			<h5 class="cl0 col-lg-12 m-t-30"><?= $user['name']; ?></h4>
			<small class="cl0 m-l-15 m-t-5"><?= $user['id']; ?></small>
		</div>
	</div>
</section>	

<!-- Content page -->
<section class="bg0 p-t-25">
	<div class="container">
		<div class="container">
			<a href="<?= base_url('voucher'); ?>" class="cl2 hov-cl1">
				<i class="fa fa-ticket"></i>
				<h5 class="dib ml-3">Voucher</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('wishlist'); ?>" class="cl2 hov-cl1">
				<i class="fa fa-heart"></i>
				<h5 class="dib ml-3">Wishlist</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('myproduct'); ?>" class="cl2 hov-cl1">
				<i class="fa fa-codepen"></i>
				<h5 class="dib ml-3">Product</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
				<hr>
			</a>

			<a href="<?= base_url('setting'); ?>" class="cl2 hov-cl1">
				<i class="fa fa-gear"></i>
				<h5 class="dib ml-3 m-b-18">Setting</h5>
				<i class="fa fa-angle-right float-right cl4 cos-size mr-1 hov-cl1"></i>
			</a>
		</div>
	</div>
</section>	