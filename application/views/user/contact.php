<!-- Alert Message -->
<?php if( $this->session->flashdata('notification') ) : ?>
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		Your message <strong>successfully</strong> <?= $this->session->flashdata('notification'); ?>!
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
<?php endif; ?>

<!-- Title page -->
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('<?= base_url(); ?>assets/user/images/bg-07.jpg');">
	<h2 class="ltext-105 cl0 txt-center">
		Contact
	</h2>
</section>	

<!-- Content page -->
<section class="bg0 p-t-120 p-b-116">
	<div class="container">
		<div class="flex-w flex-tr">
			<div class="size-210 bor10 p-lr-70 p-t-55 p-b-70 p-lr-15-lg w-full-md">
				<form action="<?= base_url('contact/create'); ?>" method="post">
					<h4 class="mtext-105 cl2 txt-center p-b-30">
						Send Us A Message
					</h4>

					<div class="bor8 how-pos4-parent">
						<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-30" type="email" name="email" placeholder="Email Address" required>
						<img class="how-pos4 pointer-none" src="<?= base_url(); ?>assets/user/images/icons/icon-email.png" alt="ICON">
					</div>
					<small class="form-text text-danger"><?= form_error('email'); ?></small>

					<div class="bor8 m-t-20 how-pos4-parent">
						<input class="stext-111 cl2 plh3 size-116 p-l-70 p-r-30" type="text" name="subject" placeholder="Subject" required>
						<img class="how-pos4 pointer-none" src="<?= base_url(); ?>assets/user/images/icons/icon-subject.png" alt="ICON">
					</div>
					<small class="form-text text-danger"><?= form_error('subject'); ?></small>

					<div class="bor8 m-t-30">
						<textarea class="stext-111 cl2 plh3 size-110 p-lr-28 p-tb-25" name="message" placeholder="How Can We Help?" required></textarea>
					</div>
					<small class="form-text text-danger m-b-10"><?= form_error('message'); ?></small>

					<button type="Submit" name="insert" class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer">
						Submit
					</button>
				</form>
			</div>

			<div class="size-210 bor10 flex-w flex-col-m p-lr-30 p-b-30 p-lr-15-lg w-full-md">
				<div class="flex-w w-full p-b-42">
					<span class="fs-18 cl5 txt-center size-211">
					</span>

					<div class="size-212 p-t-2">
						<span class="mtext-110 cl2">
							Address
						</span>

						<p class="stext-115 cl6 size-213 p-t-18">
							JL. Kopo Sayati, Sayati, Kec.Margahayu, Bandung, Jawa Barat 40228.
						</p>
					</div>
				</div>

				<div class="flex-w w-full p-b-42">
					<span class="fs-18 cl5 txt-center size-211">
					</span>

					<div class="size-212 p-t-2">
						<span class="mtext-110 cl2">
							Lets Talk
						</span>

						<p class="stext-115 cl1 size-213 p-t-18">
							+62 882 2212 7200
						</p>
					</div>
				</div>

				<div class="flex-w w-full">
					<span class="fs-18 cl5 txt-center size-211">
					</span>

					<div class="size-212 p-t-2">
						<span class="mtext-110 cl2">
							Sale Support
						</span>

						<p class="stext-115 cl1 size-213 p-t-18">
							ZStore96@gmail.com
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	


<!-- Map -->
<!-- <div class="map">
	<div class="size-303" id="google_map" data-map-x="40.691446" data-map-y="-73.886787" data-pin="images/icons/pin.png" data-scrollwhell="0" data-draggable="1" data-zoom="11"></div>
</div> -->