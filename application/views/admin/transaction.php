<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> user!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transaction</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">Transaction</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content-header">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th class="max-w-2">NO</th>
                  <th>Code</th>
                  <th>User</th>
                  <th>Address</th>
                  <th>Product</th>
                  <th>Shipping</th>
                  <th>Metode</th>
                  <th>Total</th>
                  <th class="min-w-7">Date</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach( $transaction as $t ) : ?>
                  <tr>
                    <td width="5%"><?= $no++; ?></td>
                    <td><?= $t['code']; ?></td>
                    <td>
                      <?= $t['user_id']; ?>
                      <a href="" data-toggle="modal" data-target="#modal-preview-user<?= $t['user_id']; ?>" title="Preview"><i class="fas fa-search"></i></a>
                      <div class="modal fade" id="modal-preview-user<?= $t['user_id']; ?>">
                        <div class="modal-dialog modal-xl">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Info User</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php $user = $this->User_model->getById($t['user_id']); ?>
                              <img src="<?= base_url(); ?>assets/img/<?= $user['picture']; ?>" class="border border-disable" width="100px">
                              <table class="table" border="0" cellpadding="5" cellspacing="5">
                                <tr>
                                  <th>Nama</th>
                                  <td><?= $user['name']; ?></td>
                                </tr>
                                <tr>
                                  <th>User ID</th>
                                  <td><?= $user['id']; ?></td>
                                </tr>
                                <tr>
                                  <th>Email</th>
                                  <td><?= $user['email']; ?></td>
                                </tr>
                                <tr>
                                  <th>Gender</th>
                                  <td><?= $user['gender']; ?></td>
                                </tr>
                                <tr>
                                  <th>Birthday</th>
                                  <td><?= $user['birthday']; ?></td>
                                </tr>
                              </table>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <?= $t['address_id']; ?>
                      <a href="" data-toggle="modal" data-target="#modal-preview-address<?= $t['address_id']; ?>" title="Preview"><i class="fas fa-search"></i></a>
                      <div class="modal fade" id="modal-preview-address<?= $t['address_id']; ?>">
                        <div class="modal-dialog modal-xl">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Info Address</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php $address = $this->Address_model->get($t['address_id']); ?>
                              <?php $city    = $this->Rajaongkir_model->getCity($address['city'], $address['province']); ?>
                              <table class="table" border="0" cellpadding="5" cellspacing="5">
                                <tr>
                                  <th>Nama</th>
                                  <td><?= $address['name']; ?></td>
                                </tr>
                                <tr>
                                  <th>Telephone</th>
                                  <td><?= $address['telephone']; ?></td>
                                </tr>
                                <tr>
                                  <th>Province</th>
                                  <td><?= strtoupper($city['province']); ?></td>
                                </tr>
                                <tr>
                                  <th>City</th>
                                  <td>
                                    <?php if( $city['type'] == 'Kabupaten' ) : ?>
                                      <?= 'KAB. ' .strtoupper($city['city_name']); ?>
                                    <?php else : ?>
                                      <?= strtoupper($city['city_name']); ?>
                                    <?php endif; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <th>Postal code</th>
                                  <td><?= $address['postal_code']; ?></td>
                                </tr>
                                <tr>
                                  <th>More complete</th>
                                  <td><?= $address['complete']; ?></td>
                                </tr>
                              </table>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <?= word_limiter($t['product'], 4); ?>
                      <a href="" data-toggle="modal" data-target="#modal-preview-product" title="Preview"><i class="fas fa-search"></i></a>
                      <div class="modal fade" id="modal-preview-product">
                        <div class="modal-dialog modal-xl">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Info Product</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php $prod = explode(",", $t['product']); ?>
                              <?php $qty  = explode(",", $t['qty']); ?>
                              <div class="row">
                                <?php $n=0; ?>
                                <table class="table col-6" border="0" cellpadding="5" cellspacing="5">
                                  <tr>
                                    <th>Product Name</th>
                                  </tr>
                                  <?php foreach( $prod as $p ) : ?>
                                    <tr>
                                      <td><?= $prod[$n++]; ?></td>
                                    </tr>
                                  <?php endforeach; ?>
                                </table>
                                <?php $p=0; ?>
                                <table class="table col-6" border="0" cellpadding="5" cellspacing="5">
                                  <tr>
                                    <th>qty</th>
                                  </tr>
                                  <?php foreach( $qty as $qt ) : ?>
                                    <tr>
                                      <td><?= $qty[$p++]; ?></td>
                                    </tr>
                                  <?php endforeach; ?>
                                </table>
                              </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <?php $ship = explode(",", $t['shipping']); ?>
                      <?= $ship[0]; ?>(<?= $ship[1]; ?>) | <?= $ship[2]; ?> day | Rp <?= number_format($ship[3], 0,',','.'); ?>
                    </td>
                    <td><?= $t['metode']; ?></td>
                    <td>Rp <?= number_format($t['total'], 0,',','.'); ?></td>
                    <td>
                      <?php $date = explode(",", $t['date']); ?>
                      <?= $date[0]; ?> - <?= $date[1]; ?>
                    </td>
                    <td><?= $t['description']; ?></td>
                    <td>
                      <?php $time = $this->Transaction_model->status($date[2]); ?>
                      <?php if( $time < 0 && $t['status'] != '4' ) : ?>
                        Expired
                      <?php else : ?>
                        <?php if( $t['status'] == '1' ) : ?> Pending <?php endif; ?>
                        <?php if( $t['status'] == '2' ) : ?> Paid <?php endif; ?>
                        <?php if( $t['status'] == '4' ) : ?>  Cancel <?php endif; ?>
                      <?php endif; ?>
                    </td>
                    <td class="text-center py-0 align-middle">
                      <?php if( $t['status'] != '4' ) : ?>
                        <div class="btn-group btn-group-sm">
                          <a href="<?= base_url('transaction/update/') .$t['code']; ?>" class="btn btn-danger text-white" title="cancel">cancel</a>
                        </div>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

 
