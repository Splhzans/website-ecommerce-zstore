<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> product!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Product</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">Product</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content table-responsive">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button class="btn-sm btn-primary" data-toggle="modal" data-target="#modal-snp">Add Product</button>
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive">
            <table id="tableAwesome1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Picture</th>
                  <th>Name</th>
                  <th>category</th>
                  <th>price</th>
                  <th>stock</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach($product as $prod) : ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" width="70px"></td>
                    <td><?= $prod['name']; ?></td>
                    <td><?= $prod['category']; ?></td>
                    <td>Rp<?= number_format($prod['price'], 0,',','.'); ?></td>
                    <td><?= $prod['stock']; ?></td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-info text-white" data-toggle="modal" data-target="#modal-preview<?= $prod['id']; ?>" title="Preview"><i class="fas fa-eye"></i></a>
                        <a href="" class="btn btn-success text-white" data-toggle="modal" data-target="#modal-edit<?= $prod['id']; ?>" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('product/delete/'). $prod['id']; ?>" class="btn btn-danger text-white" onclick="return confirm('Sure?');" title="Delete"><i class="fas fa-trash"></i></a>
                      </div>
                    </td>

                    <!-- modal preview product -->
                    <div class="modal fade" id="modal-preview<?= $prod['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Preview Product</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <section class="sec-product-detail bg0 p-t-65 p-b-60">
                              <div class="container">
                                <div class="row">
                                  <div class="col-md-4 col-lg-4 p-b-30 mb-3">
                                    <img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" width="100%">
                                  </div>

                                  <div class="col-md-2 col-lg-2 p-b-30">
                                    <img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" width="100%" class="mb-3">
                                    <img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" width="100%">
                                  </div>

                                  <div class="col-md-6 col-lg-6 p-b-30">
                                    <h4 class="mtext-105 cl2">
                                      <?= $prod['name']; ?>
                                    </h4>

                                    <span class="mtext-106 cl2">
                                      Rp <?= number_format($prod['price'], 0,',','.'); ?>
                                    </span>

                                    <p class="stext-102 cl3 pt-3">
                                      <?= $prod['description']; ?>
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <h6>Seller   : <small><!-- <?= $prod['seller']; ?> --></small></h6> 
                                <h6>Stock    : <small><?= $prod['stock']; ?></small></h6> 
                                <h6>Category : <small><?= $prod['category']; ?></small></h6>
                              </div>
                            </section>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- modal add product -->
    <div class="modal fade" id="modal-snp">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Product</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?= form_open_multipart('product/create'); ?>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-6">
                <label for="picture">Picture (1200x1485)</label>
                <input type="file" class="form-control-file" id="picture" name="picture" required>
              </div>
              <div class="form-group col-6">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" required>
              </div>

              <div class="form-group col-6">
                <label for="price">Price</label>
                <input type="text" name="price" class="form-control" id="price" placeholder="0" required>
              </div>

              <div class="form-group col-6">
                <label>Category</label>
                <select class="form-control" name="category" required>
                  <?php foreach( $categories as $cat ) : ?>
                    <option value="<?= $cat['category']; ?>"><?= $cat['category']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group col-6">
                <label for="stock">Stock</label>
                <input type="text" name="stock" class="form-control" id="stock" placeholder="0" required>
              </div>

              <div class="form-group col-12">
                <label for="description">Description</label>
                <textarea name="description" class="form-control" rows="3" id="description" required></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
          <?= form_close(); ?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
</div>