<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> user!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content-header">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th class="max-w-2">NO</th>
                  <th class="max-w-4">Picture</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Birthday</th>
                  <th>role</th>
                  <th class="min-w-7">Date Created</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach( $users as $user ) : ?>
                  <tr>
                    <td width="5%"><?= $no++; ?></td>
                    <td><img src="<?= base_url(); ?>assets/img/<?= $user['picture']; ?>" width="40px"></td>
                    <td><?= $user['name']; ?></td>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['gender']; ?></td>
                    <td><?= $user['birthday']; ?></td>
                    <td><?php if( $user['role_id'] == 1 ) : ?> admin <?php else : ?> member <?php endif; ?></td>
                    <td><?= $user['date_created']; ?></td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-success text-white" data-toggle="modal" data-target="#modal-edit<?= $user['id']; ?>" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('user/delete/'). $user['id']; ?>" class="btn btn-danger text-white" onclick="return confirm('Sure?');" title="Delete"><i class="fas fa-trash"></i></a>
                      </div>
                    </td>

                    <!-- modal edit user -->
                    <div class="modal fade" id="modal-edit<?= $user['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Edit User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <?= form_open_multipart('user/update'); ?>
                          <div class="modal-body">
                            <?= form_hidden('id', $user['id']); ?>
                            <?= form_hidden('img', $user['picture']); ?>
                            <div class="row">
                              <div class="form-group col-6">
                                <label for="picture">Picture</label>
                                <input type="file" class="form-control-file" id="picture" name="picture">
                              </div>
                              <div class="form-group col-6">
                                <label for="role">Role</label>
                                <select class="form-control" name="role" id="role" required>
                                  <option value="1" <?php if( $user['role_id'] == 1 ) : ?> selected <?php endif; ?>>admin</option>
                                  <option value="2" <?php if( $user['role_id'] == 2 ) : ?> selected <?php endif; ?>>member</option>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-6">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" value="<?= $user['name']; ?>" required>
                              </div>
                              <div class="form-group col-6">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" id="email" value="<?= $user['email']; ?>" required>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-6">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" id="gender" required>
                                  <option value="male" <?= $user['gender'] == 'male' ? 'selected' : ''; ?>>Male</option>
                                  <option value="female" <?= $user['gender'] == 'female' ? 'selected' : ''; ?>>Female</option>
                                </select>
                              </div>
                              <div class="form-group col-6">
                                <label for="birthday">Birthday</label>
                                <input type="date" name="birthday" class="form-control" id="birthday" value="<?= $user['birthday']; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                          <?= form_close(); ?>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

 
