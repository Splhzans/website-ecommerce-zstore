<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> banner!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Banner</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">Banner</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button class="btn-sm btn-primary" data-toggle="modal" data-target="#modal-addBanner">Add Banner</button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="20%">NO</th>
                  <th width="50%">Picture</th>
                  <th width="30%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach( $banner as $ban ) : ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><img src="<?= base_url(); ?>assets/img/<?= $ban['picture']; ?>" width="500px"></td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-info text-white" data-toggle="modal" data-target="#modal-preview<?= $ban['id']; ?>" title="Preview"><i class="fas fa-eye"></i></a>
                        <a href="" class="btn btn-success text-white" data-toggle="modal" data-target="#modal-edit<?= $ban['id']; ?>" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('banner/delete/'). $ban['id']; ?>" class="btn btn-danger text-white" onclick="return confirm('Sure?');" title="Delete"><i class="fas fa-trash"></i></a>
                      </div>
                    </td>

                    <!-- modal preview banner -->
                    <div class="modal fade" id="modal-preview<?= $ban['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Preview Banner</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <img src="<?= base_url(); ?>assets/img/<?= $ban['picture']; ?>" width="765px">
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                    <!-- modal edit banner -->
                    <div class="modal fade" id="modal-edit<?= $ban['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Edit Banner</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <?= form_open_multipart('banner/update'); ?>
                          <div class="modal-body">
                            <?= form_hidden('id', $ban['id']); ?>
                            <?= form_hidden('img', $ban['picture']); ?>
                            <div class="form-group col-6">
                              <label for="picture">Picture (1920x930)</label>
                              <input type="file" class="form-control-file" id="picture" name="picture">
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                          <?= form_close(); ?>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- modal added banner -->
    <div class="modal fade" id="modal-addBanner">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Banner</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?= form_open_multipart('banner/create'); ?>
          <div class="modal-body">
            <div class="form-group col-6">
              <label for="picture">Picture (1920x930)</label>
              <input type="file" class="form-control-file" id="picture" name="picture" required>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
          <?= form_close(); ?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
</div>

 
