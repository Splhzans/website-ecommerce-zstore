<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> news!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>News</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">News</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content table-responsive">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Picture</th>
                  <th>By</th>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach( $news as $row ) : ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><img src="<?= base_url(); ?>assets/img/<?= $row['picture']; ?>" width="120px"></td>
                    <td><?= $row['by']; ?></td>
                    <td><?= $row['title']; ?></td>
                    <td><?= $row['date']; ?></td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-info text-white" data-toggle="modal" data-target="#modal-detail<?= $row['id']; ?>" title="Preview"><i class="fas fa-eye"></i></a>
                        <a href="" class="btn btn-success text-white" data-toggle="modal" data-target="#modal-update<?= $row['id']; ?>" title="Edit"><i class="fas fa-edit"></i></a>
                      </div>
                    </td>

                    <!-- modal detail -->
                    <div class="modal fade" id="modal-detail<?= $row['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">News Detail</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <!-- Content page -->
                            <section class="bg0 p-t-52 p-b-20">
                              <div class="container">
                                <div class="row">
                                  <div class="col-md-12 col-lg-12 p-b-80">
                                    <div class="p-r-45 p-r-0-lg">
                                      <!--  -->
                                      <div class="wrap-pic-w how-pos5-parent col-12">
                                       <img src="<?= base_url(); ?>assets/img/<?= $row['picture']; ?>" alt="IMG-BLOG" class="img-size-100 mt-3">
                                      </div>

                                      <div class="p-t-32">
                                        <span>By</span> <?= $row['by']; ?> | <?= $row['date']; ?>

                                        <h4 class="mt-3">
                                          <?= $row['title']; ?>
                                        </h4>

                                        <p class="align-justify mt-4"><?= $row['contents']; ?></p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </section>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal detail -->

                    <!-- modal update -->
                    <div class="modal fade" id="modal-update<?= $row['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">News Update</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <?= form_open_multipart('newses/update'); ?>
                          <div class="modal-body">
                            <?= form_hidden('id', $row['id']); ?>
                            <?= form_hidden('img', $row['picture']); ?>
                            <div class="form-group col-6">
                              <label for="picture">Picture (1200x600)</label>
                              <input type="file" class="form-control-file" id="picture" name="picture">
                            </div>
                            <div class="row">
                              <div class="form-group col-6">
                                <label for="by">BY</label>
                                <input type="text" name="by" class="form-control" id="by" placeholder="Writer" value="<?= $row['by']; ?>">
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-6">
                                <label for="title">Title</label>
                                <textarea name="title" class="form-control" rows="3" id="title"><?= $row['title']; ?></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label>Contents</label>
                              <textarea name="contents" class="textarea"
                              style="width: 100%; height: 50px; font-size: 12px; line-height: 0px; border: 1px solid #dddddd; padding: 10px;"><?= $row['contents']; ?></textarea>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                          </div>
                          <?= form_close(); ?>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal update-->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


