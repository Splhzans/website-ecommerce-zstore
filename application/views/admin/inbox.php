<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      Message <?= $this->session->flashdata('notification'); ?> <strong>successfully!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Inbox</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">Inbox</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content table-responsive">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Feedback from user</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach($message as $inbox) : ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $inbox['email']; ?></td>
                    <td><?= word_limiter($inbox['subject'], 7); ?></td>
                    <td><?= $inbox['date']; ?></td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-primary text-white" data-toggle="modal" data-target="#modal-read<?= $inbox['id']; ?>" title="Read"><i class="fas fa-envelope-open-text"></i></a>
                        <a href="<?= base_url('inbox/delete/'). $inbox['id']; ?>" class="btn btn-danger text-white" onclick="return confirm('Sure?');" title="Delete"><i class="fas fa-trash"></i></a>
                      </div>
                    </td>

                    <!-- Modal -->
                    <div class="modal fade" id="modal-read<?= $inbox['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header bg-primary">
                            <h4 class="modal-title">Read Message</h4>
                            <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <!-- /.card-header -->
                          <div class="card-body p-0">
                            <div class="mailbox-read-info">
                              <h5><?= $inbox['subject']; ?></h5>
                              <h6>From: <?= $inbox['email']; ?>
                              <span class="mailbox-read-time float-right"><?= $inbox['date']; ?></span></h6>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <div class="mailbox-controls with-border text-center">
                              <div class="btn-group">
                                <a href="<?= base_url('inbox/delete/'). $inbox['id']; ?>">
                                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete"><i class="far fa-trash-alt"></i></button>
                                </a>
                                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply"><i class="fas fa-reply"></i></button>
                                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward"><i class="fas fa-share"></i></button>
                              </div>
                              <!-- /.btn-group -->
                              <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print"><i class="fas fa-print"></i></button>
                            </div>
                            <!-- /.mailbox-controls -->
                            <div class="mailbox-read-message">
                              <?= $inbox['message']; ?>
                            </div>
                            <!-- /.mailbox-read-message -->
                          </div>
                          <!-- /.card-body -->
                          <!-- /.card-footer -->
                          <div class="card-footer">
                            <div class="float-right">
                              <button type="button" class="btn btn-default"><i class="fas fa-reply"></i> Reply</button>
                              <button type="button" class="btn btn-default"><i class="fas fa-share"></i> Forward</button>
                            </div>
                            <a href="<?= base_url('inbox/delete/'). $inbox['id']; ?>">
                              <button type="button" class="btn btn-default"><i class="far fa-trash-alt"></i> Delete</button>
                            </a>
                            <button type="button" class="btn btn-default"><i class="fas fa-print"></i> Print</button>
                          </div>
                          <!-- /.card-footer -->
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


