<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Alert Message -->
  <?php if( $this->session->flashdata('notification') ) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Successfully</strong> <?= $this->session->flashdata('notification'); ?> category!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Categories</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
            <li class="breadcrumb-item active">Category</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content table-responsive">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button class="btn-sm btn-primary" data-toggle="modal" data-target="#modal-addCategory">Add Category</button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="tableAwesome1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; ?>
                <?php foreach( $categories as $cat ) : ?>
                  <tr>
                    <td width="10%"><?= $no++; ?></td>
                    <td><?= $cat['category']; ?></td>
                    <td width="15%" class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-success text-white" data-toggle="modal" data-target="#modal-edit<?= $cat['id']; ?>" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('category/delete/'). $cat['id']; ?>" class="btn btn-danger text-white" onclick="return confirm('Sure?');" title="Delete"><i class="fas fa-trash"></i></a>
                      </div>
                    </td>

                    <!-- modal edit category -->
                    <div class="modal fade" id="modal-edit<?= $cat['id']; ?>">
                      <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Edit Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <?= form_open_multipart('category/update'); ?>
                          <div class="modal-body">
                            <?= form_hidden('id', $cat['id']); ?>
                            <div class="form-group col-12">
                              <label for="picture">Category</label>
                              <input type="text" class="form-control" id="category" name="name" value="<?= $cat['category']; ?>">
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                          <?= form_close(); ?>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- modal add Category -->
    <div class="modal fade" id="modal-addCategory">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Category</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?= form_open_multipart('category/create'); ?>
          <div class="modal-body">
            <div class="form-group col-12">
              <label for="name">Category</label>
              <input type="text" name="name" class="form-control" id="category" placeholder="name" required>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
          <?= form_close(); ?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
</div>

 
