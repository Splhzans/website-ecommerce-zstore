<!-- Footer -->
<footer class="bg3 p-t-75 p-b-32">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-lg-1 p-b-50 p-r-130">
				<h4 class="stext-301 cl0 p-b-30">
					Categories
				</h4>

				<ul>
					<li class="p-b-10">
						<a href="<?= base_url('shop/women'); ?>" class="stext-107 cl7 hov-cl1 trans-04">
							Women
						</a>
					</li>

					<li class="p-b-10">
						<a href="<?= base_url('shop/men'); ?>" class="stext-107 cl7 hov-cl1 trans-04">
							Men
						</a>
					</li>

					<li class="p-b-10">
						<a href="<?= base_url('shop/bags'); ?>" class="stext-107 cl7 hov-cl1 trans-04">
							Bags
						</a>
					</li>

					<li class="p-b-10">
						<a href="<?= base_url('shoes'); ?>" class="stext-107 cl7 hov-cl1 trans-04">
							Shoes
						</a>
					</li>
				</ul>
			</div>

			<div class="col-sm-6 col-lg-1 p-b-50 p-r-100">
				<h4 class="stext-301 cl0 p-b-30">
					Help
				</h4>

				<ul>
					<li class="p-b-10">
						<a href="#" class="stext-107 cl7 hov-cl1 trans-04">
							Track Order
						</a>
					</li>

					<li class="p-b-10">
						<a href="#" class="stext-107 cl7 hov-cl1 trans-04">
							Returns 
						</a>
					</li>

					<li class="p-b-10">
						<a href="#" class="stext-107 cl7 hov-cl1 trans-04">
							Shipping
						</a>
					</li>

					<li class="p-b-10">
						<a href="#" class="stext-107 cl7 hov-cl1 trans-04">
							FAQs
						</a>
					</li>
				</ul>
			</div>

			<div class="col-sm-2 col-lg-3 m-r-40 p-b-30">
				<h4 class="stext-301 cl0 p-b-30">
					GET IN TOUCH
				</h4>

				<p class="stext-107 cl7 size-201">
					Any questions? JL. Kopo Sayati, Sayati, Kec.Margahayu, Bandung, Jawa Barat 40228. or call us on (+62) 882 2212 7200
				</p>

				<div class="p-t-27">
					<a href="https://m.facebook.com/muhammad.i.saepulloh.1" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
						<i class="fa fa-facebook"></i>
					</a>

					<a href="https://www.instagram.com/splhzans_/" target="_blank" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
						<i class="fa fa-instagram"></i>
					</a>

					<a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
						<i class="fa fa-pinterest-p"></i>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-3 p-b-50 p-l-15">
				<h4 class="stext-301 cl0 p-b-30">
					Newsletter
				</h4>

				<form>
					<div class="row">
						<div class="wrap-input1 w-full col-sm-12 col-lg-10">
							<input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email" placeholder="email@example.com">
							<div class="focus-input1 trans-04"></div>
						</div>
						<div class="col-lg-2 m-t-5">
						<button class="flex-c-m stext-101 cl0 size-102 bg1 bor1 hov-btn2 trans-04">
							Subscribe
						</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="p-t-40">
			<div class="flex-c-m flex-w p-b-18">
				<a href="#" class="m-all-1">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-pay-01.png" alt="ICON-PAY">
				</a>

				<a href="#" class="m-all-1">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-pay-02.png" alt="ICON-PAY">
				</a>

				<a href="#" class="m-all-1">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-pay-03.png" alt="ICON-PAY">
				</a>

				<a href="#" class="m-all-1">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-pay-04.png" alt="ICON-PAY">
				</a>

				<a href="#" class="m-all-1">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-pay-05.png" alt="ICON-PAY">
				</a>
			</div>

			<p class="stext-107 cl6 txt-center">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

			</p>
		</div>
	</div>
</footer>
