<!DOCTYPE html>
<html lang="en">
<head>
	<title>ZStore | <?= $title; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= base_url(); ?>assets/user/images/icons/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/fonts/linearicons-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/animate/animate.css">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/select2/select2.min.css">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/slick/slick.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/MagnificPopup/magnific-popup.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/vendor/perfect-scrollbar/perfect-scrollbar.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/user/css/main.css">
	<!--===============================================================================================-->
	<style type="text/css">
		.cf:before,
		.cf:after {
		    content: " "; /* 1 */
		    display: table; /* 2 */
		}
		.cf:after { clear: both; }
		.cf { *zoom: 1; }

		.pct-l { margin-left: 89.5%; }
		.cl-sb { color: #007bff; }
		.cl-r { color: #dc3545; }
		.border-bot {
			border-top: 0px;
			border-left: 0px;
			border-right: 0px;
		}
		.border-none {
			border-top: 0px;
			border-left: 0px;
			border-right: 0px;
			border-bottom: 0px;
		}
		.gender {
			float: right;
			border: none;
			margin-top: 15px;
			margin-bottom: 15px;
			margin-right: 4%;
		}
		.dib { display: inline-block; }
		.cos-size {
			font-weight: bold;
			font-size: 20px;
		}
		.select-a {
			float: right;
			border: none;
			margin: 6px 0px;
		}	
		.lt-s-1 { letter-spacing: 1px; }
		.lt-s-8 { letter-spacing: 8px; }
		.align-right {
			text-align: right;
		}
		.img-p {
			min-height: 280px;
			max-height: 280px;
			min-width: 220px;
			max-width: 220px;
		}
	</style>
</head>
<body class="animsition">
	
	<!-- Header -->
	<header class="header-v2">
		<!-- Header desktop -->
		<div class="container-menu-desktop trans-03">
			<div class="wrap-menu-desktop">
				<nav class="limiter-menu-desktop p-l-45">
					
					<!-- Logo desktop -->		
					<a href="<?= base_url('about'); ?>" class="logo">
						<img src="<?= base_url(); ?>assets/user/images/icons/logo-05.png" alt="IMG-LOGO" class="ml-1">
					</a>

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
						<?php $uri = $this->uri->segment(1); ?>
							<li class="<?= $uri == 'home' ? 'active-menu' : ''; ?>">
								<a href="<?= base_url('home'); ?>">Home</a>
							</li>

							<li class="<?= $uri == 'shop' || $uri == 'productDetail' ? 'active-menu' : ''; ?>">
								<a href="<?= base_url('shop'); ?>">Shop</a>
							</li>

							<li class="label1 <?= $uri == 'features' ? 'active-menu' : ''; ?>" data-label1="hot">
								<a href="<?= base_url('features'); ?>">Features</a>
							</li>

							<li class="<?= $uri == 'news' || $uri == 'newsDetail' ? 'active-menu' : ''; ?>">
								<a href="<?= base_url('news'); ?>">News</a>
							</li>

							<li class="<?= $uri == 'about' ? 'active-menu' : ''; ?>">
								<a href="<?= base_url('about'); ?>">About</a>
							</li>

							<li class="<?= $uri == 'contact' ? 'active-menu' : ''; ?>">
								<a href="<?= base_url('contact'); ?>">Contact</a>
							</li>
						</ul>
					</div>	

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m h-full">
						<div class="flex-c-m h-full p-r-24">
							<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 js-show-modal-search">
								<i class="zmdi zmdi-search"></i>
							</div>
						</div>

						<?php $a=0; $b=0; ?>
						<?php $row = $this->db->get('t_cart')->result_array(); ?>
						<?php foreach( $row as $item ) { if( $item['user_id'] == $this->session->userdata('id') ) { $a++; }} ?>
						<?php foreach( $this->cart->contents() as $items ) { $b++; }?>
						<?php $cart = $this->Cart_model->getAll(); ?>

						<div class="flex-c-m h-full p-l-18 p-r-25 bor5">
							<?php if( !$this->session->userdata('id') ) : ?>
								<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="<?= $b; ?>">
									<i class="zmdi zmdi-shopping-cart"></i>
								</div>
							<?php else : ?>
								<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="<?= $a; ?>">
									<i class="zmdi zmdi-shopping-cart"></i>
								</div>
							<?php endif; ?>
						</div>
						<?php $user = $this->db->get_where('t_user', ['id' => $this->session->userdata('id')])->row_array(); ?>
						<?php if( $this->session->userdata('id') ) : ?>
							<div class="flex-c-m h-full p-lr-19">
								<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 js-show-sidebar">
									<i class="zmdi zmdi-menu"></i>
									<!-- <img src="<?= base_url(); ?>/assets/img/<?= $user['picture']; ?>" class="rounded-circle border" width="35px"> -->
								</div>
							</div>
						<?php else : ?>
							<div class="flex-c-m h-full p-lr-19">
								<a href="<?= base_url('auth/login'); ?>" class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11">
									<i class="zmdi zmdi-sign-in"></i>
								</a>
							</div>
						<?php endif; ?>
					</div>
				</nav>
			</div>	
		</div>

		<!-- Header Mobile -->
		<div class="wrap-header-mobile">
			<!-- Logo moblie -->		
			<div class="logo-mobile">
				<a href="<?= base_url(); ?>user/about"><img src="<?= base_url(); ?>assets/user/images/icons/logo-05.png" alt="IMG-LOGO" class="ml-1"></a>
			</div>

			<!-- Icon header -->
			<div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
				<div class="flex-c-m h-full p-r-10">
					<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 js-show-modal-search">
						<i class="zmdi zmdi-search"></i>
					</div>
				</div>

				<div class="flex-c-m h-full p-lr-10 bor5">
					<?php if( !$this->session->userdata('id') ) : ?>
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="<?= $b; ?>">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>
					<?php else : ?>
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="<?= $a; ?>">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>


		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="main-menu-m">
				<li>
					<a href="<?= base_url('home'); ?>">Home</a>
				</li>

				<li>
					<a href="<?= base_url('shop'); ?>">Shop</a>
				</li>

				<li>
					<a href="<?= base_url('features'); ?>" class="label1 rs1" data-label1="hot">Features</a>
				</li>

				<li>
					<a href="<?= base_url('news'); ?>">News</a>
				</li>

				<li>
					<a href="<?= base_url('about'); ?>">About</a>
				</li>

				<li>
					<a href="<?= base_url('contact'); ?>">Contact</a>
				</li>
			</ul>
		</div>

		<!-- Modal Search -->
		<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
			<div class="container-search-header">
				<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
					<img src="<?= base_url(); ?>assets/user/images/icons/icon-close2.png" alt="CLOSE">
				</button>

				<form action="<?= base_url('shop'); ?>" method="post" class="wrap-search-header flex-w p-l-15">
					<button class="flex-c-m trans-04">
						<i class="zmdi zmdi-search"></i>
					</button>
					<input class="plh3" type="text" name="keyword" placeholder="Search...">
				</form>
			</div>
		</div>
	</header>

	<!-- Cart -->
	<?php if( !$this->session->userdata('id') ) : ?>
		<div class="wrap-header-cart js-panel-cart">
			<div class="s-full js-hide-cart"></div>

			<div class="header-cart flex-col-l p-l-65 p-r-25">
				<div class="header-cart-title flex-w flex-sb-m p-b-8">
					<span class="mtext-103 cl2">
						Your Cart
					</span>

					<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
						<i class="zmdi zmdi-close"></i>
					</div>
				</div>
				
				<?php if( !$this->cart->contents() ) : ?>
					<h3 class="mx-auto text-muted m-t-105 m-b-25"><i>your cart is empty</i></h3>
					<a href="<?= base_url('shop'); ?>" class="btn btn-success mx-auto">Shop Now</a>
				<?php else : ?>

				<div class="header-cart-content flex-w js-pscroll">
					<ul class="header-cart-wrapitem w-full">
						<?php foreach( $this->cart->contents() as $item ) : ?>
							<li class="header-cart-item flex-w flex-t m-b-12">
								<div class="header-cart-item-img">
									<img src="<?= base_url(); ?>assets/img/<?= $item['image']; ?>" alt="IMG">
								</div>

								<div class="header-cart-item-txt p-t-8">
									<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
										<?= $item['name']; ?>
									</a>

									<span class="header-cart-item-info">
										<?= $item['qty'] . ' x Rp ' . number_format($item['price'], '0',',','.'); ?>
									</span>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
					
					<div class="w-full">
						<div class="header-cart-total w-full p-tb-40">
							Total: Rp <?= number_format($this->cart->total(), '0',',','.'); ?>
						</div>

						<div class="header-cart-buttons flex-w w-full">
							<a href="<?= base_url('features'); ?>" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
								View Cart
							</a>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	<?php else : ?>
		<div class="wrap-header-cart js-panel-cart">
			<div class="s-full js-hide-cart"></div>

			<div class="header-cart flex-col-l p-l-65 p-r-25">
				<div class="header-cart-title flex-w flex-sb-m p-b-8">
					<span class="mtext-103 cl2">
						Your Cart
					</span>

					<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
						<i class="zmdi zmdi-close"></i>
					</div>
				</div>
				
				<?php $check = $this->Cart_model->getUser(); ?>
				<?php if( !$check ) : ?>
					<h3 class="mx-auto text-muted m-t-105 m-b-25"><i>your cart is empty</i></h3>
					<a href="<?= base_url('shop'); ?>" class="btn btn-success mx-auto">Shop Now</a>
				<?php else : ?>

				<div class="header-cart-content flex-w js-pscroll">
					<ul class="header-cart-wrapitem w-full">
						<?php $price=0; ?>
						<?php foreach( $cart as $prod ) : ?>
							<?php if( $prod['user_id'] == $this->session->userdata('id') ) : ?>
								<?php $price += $prod['qty'] * $prod['price']; ?>
								<li class="header-cart-item flex-w flex-t m-b-12">
									<div class="header-cart-item-img">
										<img src="<?= base_url(); ?>assets/img/<?= $prod['picture']; ?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt p-t-8">
										<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
											<?= $prod['name']; ?>
										</a>

										<span class="header-cart-item-info">
											<?= $prod['qty'] . ' x Rp ' . number_format($prod['price'], '0',',','.'); ?>
										</span>
									</div>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
					
					<div class="w-full">
						<div class="header-cart-total w-full p-tb-40">
							Total: Rp <?= number_format($price, '0',',','.'); ?>
						</div>

						<div class="header-cart-buttons flex-w w-full">
							<a href="<?= base_url('features'); ?>" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
								View Cart
							</a>

							<a href="<?= base_url('features/checkout'); ?>" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
								Check Out
							</a>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>