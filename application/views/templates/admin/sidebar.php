<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="£" class="brand-link">
    <img src="<?= base_url(); ?>assets/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"S>
    <span class="brand-text font-weight-light">Admin ZStore</span>
  </a>

  <?php $user = $this->db->get_where('t_user', ['id' => $this->session->userdata('id')])->row_array(); ?>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?= base_url(); ?>assets/img/<?= $user['picture']; ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?= $user['name']; ?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="<?= base_url(); ?>dashboard" class="nav-link <?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>banner" class="nav-link <?= $this->uri->segment(1) == 'banner' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-flag"></i>
              <p>
                Banner
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>user" class="nav-link <?= $this->uri->segment(1) == 'user' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>product" class="nav-link <?= $this->uri->segment(1) == 'product' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Product
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>category" class="nav-link <?= $this->uri->segment(1) == 'category' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Categories
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>transaction" class="nav-link <?= $this->uri->segment(1) == 'transaction' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-hand-holding-usd"></i>
              <p>
                Transaction
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>newses" class="nav-link <?= $this->uri->segment(1) == 'newses' ? 'active' : ''; ?>">
              <i class="nav-icon far fa-newspaper"></i>
              <p>
                News
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>about" class="nav-link <?= $this->uri->segment(1) == 'about' ? 'active' : ''; ?>">
              <i class="nav-icon fas fa-info-circle"></i>
              <p>
                About
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url(); ?>inbox" class="nav-link <?= $this->uri->segment(1) == 'inbox' ? 'active' : ''; ?>">
              <i class="nav-icon fa fa-inbox"></i>
              <p>
                Inbox
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('auth/logout'); ?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>