<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_category')->result_array();
	}


	// INSERT

	public function create()
	{
		$data = [
			"category" => $this->input->post('name', true)
		];

		$this->db->insert('t_category', $data);
	}


	// UPDATE

	public function update()
	{
		$data = [
			"category"      => $this->input->post('name', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_category', $data);
	}


	// DELETE

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_category');
	}
}