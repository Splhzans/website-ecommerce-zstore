<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_product')->result_array();
	}

	public function get($id)
	{
		return $this->db->get_where('t_product', ['id' => $id])->row_array();
	}

	public function getUser()
	{
		$id = $this->session->userdata('id');
		return $this->db->get_where('t_product', ['seller_id' => $id])->result_array();
	}

	public function getCategory($cat)
	{
		return $this->db->get_where('t_product', ['category' => $cat])->result_array();
	}

	public function getManyWhere($key)
	{
		$this->db->select('*');
		$this->db->from('t_product');
		$this->db->like('name', $key);
		$keyword = $this->db->get();
		return $keyword->result_array();
	}


	// INSERT

	public function create()
	{
		$picture = $_FILES['picture'];

		if( $picture == '' ) {} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {
				echo "<script>
					alert('Upload Gambar Gagal!');
					window.location.href='product';
				</script>";

			} else {
				$picture = $this->upload->data('file_name');
			}
		}

		$data = [
			"picture"      => $picture,
			"name"         => $this->input->post('name', true),
			"seller_id"    => $this->input->post('seller'),
			"price"        => $this->input->post('price', true),
			"stock"        => $this->input->post('stock', true),
			"category"     => $this->input->post('category', true),
			"description"  => $this->input->post('description', true)
		];

		$this->db->insert('t_product', $data);
	}


	// UPDATE
	
	public function update()
	{
		$picture = $_FILES['picture']['name'];

		if( $picture == '' ) {
			$picture = $this->input->post('img');
		} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {} else {
				$picture = $this->upload->data('file_name');
			}
		}

		$data = [
			"picture"      => $picture,
			"name"         => $this->input->post('name', true),
			"seller_id"    => $this->input->post('seller'),
			"price"        => $this->input->post('price', true),
			"stock"        => $this->input->post('stock', true),
			"category"     => $this->input->post('category', true),
			"description"  => $this->input->post('description', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_product', $data);
	}


	// DELETE

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_product');
	}
}