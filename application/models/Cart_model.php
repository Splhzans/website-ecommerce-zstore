<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_model extends CI_Model {
	public function getAll()
	{
		$this->db->select('*');
		$this->db->from('t_cart');
		$this->db->join('t_product','t_product.id = t_cart.product_id');
		return $cart = $this->db->get()->result_array();
	}

	public function getUser()
	{
		return $this->db->get_where('t_cart', ['user_id' => $this->session->userdata('id')])->result_array();
	}


	// INSERT

	public function create()
	{
		if( !$this->session->userdata('id') ) {
			$data = array(
		        'id'         =>  $this->input->post('id'),
		        'image'      =>  $this->input->post('image'),
		        'qty'        =>  $this->input->post('qty'),
		        'name'       =>  $this->input->post('name'),
		        'price'      =>  $this->input->post('price')
			);

			$this->cart->insert($data);
		} else {
			if( !$this->session->flashdata('migrate') ) {
				$row = $this->db->get_where('t_cart', ['user_id' => $this->session->userdata('id'), 'product_id' => $this->input->post('id')])->row_array();
				if( !$row ) {
					$data = array(
						'user_id'    => $this->session->userdata('id'),
						'product_id' => $this->input->post('id'),
					    'qty'        => $this->input->post('qty')
					);
					$this->db->insert('t_cart', $data);
					echo "<script> history.go(-1); </script>";
				} else {
					$qty = $row['qty'] + $this->input->post('qty');
					$data = array(
						'user_id'    => $this->session->userdata('id'),
						'product_id' => $this->input->post('id'),
					    'qty'        => $qty
					);
					$this->db->where('user_id', $this->session->userdata('id'));
					$this->db->where('product_id', $this->input->post('id'));
					$this->db->update('t_cart', $data);

					echo "<script> history.go(-1); </script>";
				}
			} else {
				foreach( $this->cart->contents() as $items ) {
					$id  = $items['id'];
					$row = $this->db->get_where('t_cart', ['user_id' => $this->session->userdata('id'), 'product_id' => $id])->row_array();
					if( !$row ) {
						$data = array(
							'user_id'    => $this->session->userdata('id'),
							'product_id' => $id,
						    'qty'        => $items['qty']
						);
						$this->db->insert('t_cart', $data);
					} else {
						$qty = $row['qty'] + $items['qty'];
						$data = array(
							'user_id'    => $this->session->userdata('id'),
							'product_id' => $id,
						    'qty'        => $qty
						);
						$this->db->where('user_id', $this->session->userdata('id'));
						$this->db->where('product_id', $id);
						$this->db->update('t_cart', $data);
					}
				}
				$this->cart->destroy();
				redirect('home');
			}
		}
	}


	// UPDATE 

	public function update()
	{
		foreach( $this->cart->contents() as $items ) {
			$data = [
			        'rowid' => $items['rowid'],
			        'qty'   => $this->input->post('qty['.$items['rowid'].']')
			];
			$this->cart->update($data);
		}
	}
	
	public function updateLog()
	{
		$n=1;
		$cart = $this->Cart_model->getAll();
		foreach( $cart as $prod ) {
			if( $this->input->post('qty['.$n.']') == 0 ) { $this->deleteLog($this->session->userdata('id'), $this->input->post('id['.$n.']')); }
			$data = [
			        'qty'   => $this->input->post('qty['.$n.']')
			];
			$this->db->where('user_id', $this->session->userdata('id'));
			$this->db->where('product_id', $this->input->post('id['.$n.']'));
			$this->db->update('t_cart', $data);
			$n++;
		}
	}


	// DELETE

	public function delete($rowid)
	{
		$this->cart->remove($rowid);
	}

	public function deleteLog($user_id, $prod_id)
	{
		$this->db->delete('t_cart', array('user_id' => $user_id, 'product_id' => $prod_id));
	}

	public function deleteLogAll()
	{
		$user_id = $this->session->userdata('id');
		$this->db->delete('t_cart', array('user_id' => $user_id));
	}

	public function total()
	{
		$data = $this->getUser();
		$total = 0;

		foreach( $data as $d ) {
			$prod = $this->Product_model->get($d['product_id']);

			$total += $prod['price'] * $d['qty'];
		}

		return $total;
	}
}