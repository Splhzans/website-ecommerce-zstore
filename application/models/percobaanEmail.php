<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {
	public function login()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if( $this->form_validation->run() == false )  {
			$data['title'] = 'Login';
			$this->load->view('templates/admin/header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/admin/footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$email 	  = htmlspecialchars($this->input->post('email'));
		$password = htmlspecialchars($this->input->post('password'));

		$user = $this->db->get_where('t_user', ['email' => $email])->row_array();
		
		if( !$user ) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Email Tidak Terdaftar</div>');
			redirect('auth/login');

		} else {
			if( !$user['is_active'] == 1 ) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Email Tidak Aktif</div>');
				redirect('auth/login');	

			} else {
				if( !password_verify($password, $user['password']) ) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Password Salah</div>');
					redirect('auth/login');	

				} else {
					$data = [
						'id'      => $user['id'],
						'role_id' => $user['role_id']
					];

					$this->session->set_userdata($data);
					if( $user['role_id'] == 1 ) {
						redirect('dashboard');

					} else {
						$this->session->set_flashdata('notif', 'Login');
						redirect('home');
					}
				}
			}
		}
	}

	public function registration()
	{
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.email]', [
			'is_unique' => 'This email has already registered.'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|min_length[8]|matches[password1]', [
			'matches'    => 'The password does not match.',
			'min_length' => 'Password must be 8 characters.'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Registration';
			$this->load->view('templates/admin/header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/admin/footer');
		} else {
			$data = [
				'picture'      => 'default.jpg',
				'name'         => htmlspecialchars($this->input->post('name', true)),
				'email'        => htmlspecialchars($this->input->post('email', true)),
				'password'     => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id'      => 2,
				'is_active'    => 0,
				'date_created' => mdate('%d %M %Y %h:%i %A')

			];

			// token

			$token = base64_encode(random_bytes(6));
			$user_token = [
				'email' => $this->input->post('email'),
				'token' => $token,
				'date_created' => time()
			];

			$this->db->insert('t_user', $data);
			$this->db->insert('t_user_token', $user_token);

			$this->_sendEmail($token, 'verify');

			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">registration sucessfully</div>');
			redirect('auth/login');
		}
	}

	private function _sendEmail($token, $type)
	{
		$email  = $this->input->post('email'); 
		$config = [
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'zstore96@gmail.com',
			'smtp_pass' => 'Persib1933',
			'smtp_port' => 465,
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'newline'   => "\r\n"
		];

		$this->load->library('email', $config);
		$this->email->initialiZe($config);

		$this->email->from('zstore96@gmail.com', 'ZStore');
		$this->email->to('mihsans972@gmail.com');
		$this->email->subject('Account Verification');
		$this->email->message('Click this link to verify you account : <a href="' .base_url(). 'auth/verify?email=' .$email .'&token=' .urldecode($token) .'">Activate</a>');

		if( $this->email->send() ) {
			return true;
		} else {
			echo $this->email->print_debugger();
			die;
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_there('t_user', ['email' => $email])->row_array();

		if( !$user ) {
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Account activation failed! Wrong email.</div>');
			redirect('auth/login');

		} else {
			$user_token = $this->db->get_where('t_user_token', ['token' => $token])->row_array();

			if( !$user_token ) {
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Account activation failed! Wrong token.</div>');
				redirect('auth/login');

			} else {
				if( time() - $user_token['date_created'] < (60 * 60 * 24) ) {
					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('user');

					$this->db->delete('t_user_token', ['email' => $email]);

				} else {
					$this->db->delete('t_user', ['email' => $email]);
					$this->db->delete('t_user_token', ['email' => $email]);

					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Account activation failed! Token expired.</div>');
					redirect('auth/login');
				}
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('role_id');

		$this->session->set_flashdata('notif', 'Logout');
	}

	public function blocked()
	{
		$this->load->view('auth/blocked');
	}
}