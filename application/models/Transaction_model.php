<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_transaction')->result_array();
	}

	public function get($code)
	{
		return $this->db->get_where('t_transaction', ['code' => $code])->row_array();
	}

	public function getCode()
	{
		$this->db->select('code');
		$this->db->from('t_transaction');
		$query = $this->db->get()->result_array();

		return $query;
	}

	public function create($code)
	{
		$time = mdate('%d %M %Y %h:%i %A');
		$ntime = strtotime($time);
		$newtime = 3600 * 24 + $ntime;
		$times = mdate('%d %M %Y %h:%i %A', $newtime);
		$realtime = $time.','.$times.','.$newtime;

		$data = [
			'user_id'      => $this->session->userdata('id'),
			'address_id'   => $this->input->post('address'),
			'product'      => $this->product(),
			'qty'          => $this->qty(),
			'shipping'     => $this->input->post('shipping'),
			'metode'       => $this->input->post('metode'),
			'total'        => $this->input->post('total'),
			'date'         => $realtime,
			'code'         => $code,
			'description'  => $this->input->post('description'),
			'status'       => '1'
		];
		

		$this->db->insert('t_transaction', $data);
	}

	public function update($code)
	{
		$data = [
			'status'       => '4'
		];

		$this->db->where('code', $code);
		$this->db->update('t_transaction', $data);
	}

	public function code()
	{
		$data = $this->getCode();

		if( !$data )
		{
			$karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789123456789';
		    $code   = 'ZS';
		    for ($i = 0; $i < 10; $i++) {
			  $pos = rand(0, strlen($karakter)-1);
			  $code .= $karakter{$pos};
			}

			return $code;
		}

	    foreach( $data as $c ) {
	    	$karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789123456789';
		    $code   = 'ZS';
		    for ($i = 0; $i < 10; $i++) {
			  $pos = rand(0, strlen($karakter)-1);
			  $code .= $karakter{$pos};
			}

			if( $c['code'] != $code ) {
	    		return $code;
	    		die();
	    	}
	    }
	}

	public function product()
	{
		$data    = $this->Cart_model->getUser();
		$product = "";

		foreach( $data as $d ) {
			$prod = $this->Product_model->get($d['product_id']);

			$product .= $prod['name'].',';
		}

		return $product;
	}

	public function qty()
	{
		$data    = $this->Cart_model->getUser();
		$qty = "";

		foreach( $data as $d ) {
			$qty .= $d['qty'].',';
		}

		return $qty;
	}

	public function status($time)
	{
		$ntime = $time - time();
		return $ntime;
	}
}