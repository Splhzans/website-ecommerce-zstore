<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_Model {
	public function getAll()
	{
		$id = $this->session->userdata('id');
		return $this->db->get_where('t_address', ['user_id' => $id])->result_array();
	}

	public function get($id)
	{
		return $this->db->get_where('t_address', ['id' => $id])->row_array();
	}

	public function create()
	{
		$province = explode("/", $this->input->post('province', true));
		$data = [
			'user_id'     => $this->session->userdata('id'),
			'name'        => htmlspecialchars($this->input->post('name', true)),
			'telephone'   => $this->input->post('telephone', true),
			'province'    => $province[6],
			'city'        => $this->input->post('city', true),
			'postal_code' => $this->input->post('code', true),
			'complete'    => $this->input->post('complete', true)
		];

		$this->db->insert('t_address', $data);
	}

	public function update()
	{
		$province = explode("/", $this->input->post('province', true));
		$data = [
			'user_id'     => $this->session->userdata('id'),
			'name'        => htmlspecialchars($this->input->post('name', true)),
			'telephone'   => $this->input->post('telephone', true),
			'province'    => $province[6],
			'city'        => $this->input->post('city', true),
			'postal_code' => $this->input->post('code', true),
			'complete'    => $this->input->post('complete', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_address', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_address');
	}
}