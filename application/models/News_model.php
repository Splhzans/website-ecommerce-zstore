<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_news')->result_array();
	}

	public function get($id)
	{
		return $this->db->get_where('t_news', ['id' => $id])->row_array();
	}


	// UPDATE

	public function update()
	{
		$picture = $_FILES['picture']['name'];

		if( $picture == '' ) {
			$picture = $this->input->post('img');
		} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {} else {
				$picture = $this->upload->data('file_name');
			}
		}

		$data = [
			"picture"       => $picture,
			"by"            => $this->input->post('by', true),
			"date"          => mdate('%d %M %Y'),
			"title"         => $this->input->post('title', true),
			"contents   "   => $this->input->post('contents', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_news', $data);
	}
}