<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist_model extends CI_Model {
	public function getAll()
	{
		$this->db->select('*');
		$this->db->from('t_wishlist');
		$this->db->join('t_product','t_product.id = t_wishlist.product_id');
		$wishlist = $this->db->get();
		return $wishlist->result_array();
	}


	// INSERT

	public function create($user, $product)
	{
		$data = [
			"user_id"    => $user,
			"product_id" => $product
		];

		$this->db->insert('t_wishlist', $data);
		echo "<script> history.go(-1); </script>";
	}


	// UPDATE

	public function delete($user_id, $prod_id)
	{
		$this->db->delete('t_wishlist', array('user_id' => $user_id, 'product_id' => $prod_id));
		echo "<script> history.go(-1); </script>";
	}
}