<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
	public function AddVisitor()
	{
		$v = $this->db->get_where('t_visitor', ['id' => 1])->row_array();
		$visitor = $v['total'] + 1;

		$data = [
			'total'       => $visitor
		];

		$this->db->where('id', 1);
		$this->db->update('t_visitor', $data);
	}

	public function getOrder()
	{
		$order = $this->db->get('t_transaction')->result_array();
		$n=0;
		foreach( $order as $o ) {
			$n += 1;
		}
		return $n;
	}

	public function getProduct()
	{
		$prod = $this->db->get('t_product')->result_array();
		$n=0;
		foreach( $prod as $p ) {
			$n += 1;
		}
		return $n;
	}

	public function getUser()
	{
		$user = $this->db->get('t_user')->result_array();
		$n=0;
		foreach( $user as $u ) {
			$n += 1;
		}
		return $n;
	}

	public function getVisitor()
	{
		$user = $this->db->get('t_visitor')->row_array();
		return $user['total'];
	}
}