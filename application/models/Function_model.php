<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Function_model extends CI_Model {
	public function admin($view, $data)
	{
		$this->load->view('templates/admin/header', $data);
		$this->load->view('templates/admin/sidebar');
		$this->load->view('admin/'. $view);
		$this->load->view('templates/admin/footer');
	}

	public function user($view, $data)
	{
		$this->load->view('templates/user/header', $data);
		$this->load->view('templates/user/sidebar');
		$this->load->view('user/'. $view);
		$this->load->view('templates/user/footer');
		$this->load->view('templates/user/close');
	}


	// public function get($table, $id)
	// {
	// 	return $this->db->get_where($table, ['id' => $id])->row_array();
	// }

	// public function getAll($table)
	// {
	// 	return $this->db->get($table)->result_array();
	// }

	// public function add($table, $data)
	// {
	// 	$this->db->insert($table, $data);
	// }

	// public function edit($table, $data, $where)
	// {
	// 	$this->db->where('id', $where);
	// 	$this->db->update($table, $data);
	// }

	// public function delete($table, $where)
	// {
	// 	$this->db->where('id', $where);
	// 	$this->db->delete($table);
	// }
}