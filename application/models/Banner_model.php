<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_banner')->result_array();
	}


	// INSERT

	public function create()
	{
		$picture = $_FILES['picture'];

		if( $picture == '' ) {} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {
				echo "<script>
					alert('Upload Gambar Gagal!');
					window.location.href='product';
				</script>";

			} else {
				$picture = $this->upload->data('file_name');
			}
		}

		$data = [
			"picture"      => $picture
		];

		$this->db->insert('t_banner', $data);
	}


	// UPDATE

	public function update()
	{
		$picture = $_FILES['picture']['name'];

		if( $picture == '' ) {
			$picture = $this->input->post('img');
		} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {} else {
				$picture = $this->upload->data('file_name');
			}
		}

		$data = [
			"picture"      => $picture,
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_banner', $data);
	}


	// DELETE

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_banner');
	}
}