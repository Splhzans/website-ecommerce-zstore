<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends CI_Model {
	public function find($id)
	{
		$result = $this->db->where('id', $id)->limit(1)->get('t_product');
		
		if($result->num_rows() > 0) {
			return $result->row();
		} else {
			return array();
		}
	}
} 