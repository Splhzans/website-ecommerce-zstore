<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_feedback')->result_array();
	}

	public function create()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() == FALSE)
        {
			redirect('contact'); 
        }
        else
        {
            $this->_create();
			$this->session->set_flashdata('notification', 'sent');
            redirect('contact');
        }

	}

	public function _create()
	{
		$data = [
			"email"   => $this->input->post('email', true),
			"subject" => $this->input->post('subject', true),
			"message" => $this->input->post('message', true),
			"date"    => mdate('%d %M. %Y %h:%i %A')
		];

		$this->db->insert('t_feedback', $data);
	}


	// DELETE

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_feedback');
	}
}