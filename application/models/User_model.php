<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	public function getAll()
	{
		return $this->db->get('t_user')->result_array();
	}

	public function get()
	{
		$id = $this->session->userdata('id');
		return $this->db->get_where('t_user', ['id' => $id])->row_array();
	}

	public function getById($id)
	{
		return $this->db->get_where('t_user', ['id' => $id])->row_array();
	}


	// UPDATE

	public function update()
	{
		$picture = $_FILES['picture']['name'];

		if( $picture == '' ) {
			$picture = $this->input->post('img');
		} else {
			$config['upload_path']          = 'assets/img';
	        $config['allowed_types']        = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('picture')) {} else {
				$picture = $this->upload->data('file_name');
			}
		}

		if( !$this->input->post('password1') ) {
			$data = [
				"picture"  => $picture,
				"name"     => $this->input->post('name', true),
				"email"    => $this->input->post('email', true),
				'gender'   => $this->input->post('gender', true),
				'birthday' => $this->input->post('birthday'),
				"role_id"  => $this->input->post('role', true)
			];
		} else {
			$data = [
				"picture"  => $picture,
				"name"     => $this->input->post('name', true),
				"email"    => $this->input->post('email', true),
				'gender'   => $this->input->post('gender', true),
				'birthday' => $this->input->post('birthday'),
				"role_id"  => $this->input->post('role', true),
				"password" => password_hash($this->input->post('password1'), PASSWORD_DEFAULT)
			];
		}

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('t_user', $data);
	}

	public function updateEmail()
	{
		$user     = $this->User_model->get();
		$password = htmlspecialchars($this->input->post('password'));

		if( !password_verify($password, $user['password']) ) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show p-l-42" role="alert">Password Salah</div>');
			redirect('email');
		} else {
			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.email]', [
				'is_unique' => 'This email has already registered.'
			]);

			if( $this->form_validation->run() == false ) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show p-l-42" role="alert">'. form_error('email') .'</div>');
				redirect('email');
			} else {
				$data = [
					"email" => htmlspecialchars($this->input->post('email', true))
				];

				$this->db->where('id', $this->input->post('id'));
				$this->db->update('t_user', $data);
			}
		}
	}

	public function updatePass()
	{
		$user  = $this->User_model->get();
		$password = htmlspecialchars($this->input->post('current'));

		if( !password_verify($password, $user['password']) ) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show p-l-42" role="alert">Password Salah</div>');
			redirect('password');
		} else {
			$this->form_validation->set_rules('repeat', 'Password', 'required|trim|min_length[8]|matches[password]');
			$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[8]|matches[repeat]', [
				'matches'    => 'The password does not match.',
				'min_length' => 'Password must be 8 characters.'
			]);

			if( $this->form_validation->run() == false ) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show p-l-42" role="alert">'. form_error('password') .'</div>');
				redirect('password');
			} else {
				$data = [
					"password" => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
				];

				$this->db->where('id', $this->input->post('id'));
				$this->db->update('t_user', $data);
			}
		}
	}


	// DELETE

	public function delete($id)
	{
		$this->db->delete('t_user', array('id' => $id));
		
		$tables = array('t_cart', 't_wishlist', 't_address');
		$this->db->where('user_id', $id);
		$this->db->delete($tables);
	}
}