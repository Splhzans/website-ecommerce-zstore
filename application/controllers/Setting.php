<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title'] = 'Setting';
		$data['user']  = $this->User_model->get();

		$this->Function_model->user('setting', $data);  
	}
}