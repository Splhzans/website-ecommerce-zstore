<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title'] = 'Contact';

		$this->Function_model->user('contact', $data);  
	}

	public function create()
	{
		$this->Message_model->create();
	}
}