<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newses extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title'] = 'News';
		$data['news']  = $this->News_model->getAll();

		$this->Function_model->admin('news', $data);
	}

	public function update()
	{
		$this->News_model->Update();
		$this->session->set_flashdata('notification', 'edit');
		redirect('newses');
	}
}