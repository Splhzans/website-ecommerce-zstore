<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']       = 'Transaction';
		$data['transaction'] = $this->Transaction_model->getAll();

		$this->Function_model->admin('transaction', $data);
	}

	public function update($code)
	{
		$this->Transaction_model->update($code);
		$this->session->set_flashdata('notification', 'cancel');
		redirect('transaction');
	}
}