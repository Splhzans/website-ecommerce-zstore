<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title']   = 'Setting';
		$data['user']    = $this->User_model->get();
		$data['address'] = $this->Address_model->getAll();


		$this->Function_model->user('address', $data);  
	}

	public function add()
	{
		$p = $this->uri->segment(3);
		$data['check'] = 0;
		if( $p ) {
			$data['check'] = $p;
			$data['city']  = $this->Rajaongkir_model->getAllCity($p);
		}

		$data['title']    = 'Setting';
		$data['user']     = $this->User_model->get();
		$data['province'] = $this->Rajaongkir_model->getAllProvince();


		$this->Function_model->user('create_address', $data);  
	}

	public function create()
	{
		$this->Address_model->create();
		$this->session->set_flashdata('notification', 'added');
		redirect('address');
	}

	public function edit($id)
	{
		$p = $this->uri->segment(4);
		$data['check'] = 0;
		if( $p ) {
			$data['check'] = $p;
			$data['city']  = $this->Rajaongkir_model->getAllCity($p);
		}

		$data['title']    = 'Setting';
		$data['user']     = $this->User_model->get();
		$data['province'] = $this->Rajaongkir_model->getAllProvince();
		$data['address']  = $this->Address_model->get($id);


		$this->Function_model->user('update_address', $data);  
	}

	public function update()
	{
		$this->Address_model->update();
		$this->session->set_flashdata('notification', 'update');
		redirect('address');
	}

	public function delete($id)
	{
		$this->Address_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('address');
	}
}