<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']   = 'Dashboard';
		$data['order']   = $this->Dashboard_model->getOrder();
		$data['product'] = $this->Dashboard_model->getProduct();
		$data['user']    = $this->Dashboard_model->getUser();
		$data['visitor'] = $this->Dashboard_model->getVisitor();

		$this->Function_model->admin('dashboard', $data);
	}
}