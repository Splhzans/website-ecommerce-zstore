<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title'] = 'Setting';
		$data['user']  = $this->User_model->get();

		$this->Function_model->user('email', $data);  
	}

	public function update()
	{
		$this->User_model->updateEmail();
		$this->session->set_flashdata('notification', 'email');
		redirect('setting');
	}
}