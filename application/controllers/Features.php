<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{ 
		$data['title']   = 'Features';
		$data['product'] = $this->Product_model->getAll();
		$data['cart']    = $this->Cart_model->getAll();
		$data['check']   = $this->Cart_model->getUser();
		$data['address'] = $this->Address_model->getAll();
		$data['total']   = $this->Cart_model->total();

		$this->session->set_flashdata('checkout', 'ok');
		$this->Function_model->user('features', $data);
	}

	public function checkout()
	{
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		$id = $this->uri->segment(3);

		if( !$id) {
			$data['title']   = 'Features';
			$data['user']    = $this->User_model->get();
			$data['total']   = $this->Cart_model->total();
			$data['address'] = $this->Address_model->getAll();
		} else {
			$address = $this->Address_model->get($id);
			$this->session->set_flashdata('checkout', 'ok');

			$data['title']   = 'Checkout';
			$data['user']    = $this->User_model->get();
			$data['total']   = $this->Cart_model->total();
			$data['address'] = $this->Address_model->getAll();
			$data['jne']     = $this->Rajaongkir_model->cost($address['city'], 'jne');
			$data['tiki']    = $this->Rajaongkir_model->cost($address['city'], 'tiki');
			$data['pos']     = $this->Rajaongkir_model->cost($address['city'], 'pos');
		}

		$this->Function_model->user('checkout', $data);  
		
	}

	public function metode()
	{
		if( !$this->session->userdata('id') ) { redirect('auth/login'); die(); }
		if( !$this->session->flashdata('checkout') ) { echo "<script> history.go(-1) </script>"; die(); }
		if( !$this->input->post('ongkir') ) { echo "<script> history.go(-1) </script>"; die(); }

		$address = explode("/", $this->input->post('address', true));
		$ongkir  = explode(",", $this->input->post('ongkir'));

		$data['title']    = 'Features';
		$data['address']  = $address[6];
		$data['subtotal'] = $this->input->post('subtotal');
		$data['ongkir']   = $this->input->post('ongkir');
		$data['total']    = $this->input->post('subtotal') + $ongkir[3];

		$this->session->set_flashdata('metode', 'ok');
		$this->Function_model->user('checkouts', $data);  
	}

	public function payment()
	{
		if( !$this->session->userdata('id') ) { redirect('auth/login'); die(); }
		if( !$this->session->flashdata('metode') ) { echo "<script> history.go(-1) </script>"; die(); }

		$code = $this->Transaction_model->code();

		$this->Transaction_model->create($code);
		$this->Cart_model->deleteLogAll();

		$data['title']       = 'Features';
		$data['transaction'] = $this->Transaction_model->get($code);

		$this->Function_model->user('receipt', $data); 
	}
}