<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->uri->segment(2) == 'logout' ) { $this->logout(); }
		if( $this->uri->segment(2) == 'blocked' ) { $this->blocked(); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('home'); }
	}

	public function login()
	{
		$this->Auth_model->login();
	}

	public function registration()
	{
		$this->Auth_model->registration();
	}

	public function logout()
	{
		$this->Auth_model->logout();
		redirect('home');
	}

	public function blocked()
	{
		$this->Auth_model->blocked();
	}
}