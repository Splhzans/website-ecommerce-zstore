<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']      = 'Product';
		$data['categories'] = $this->Category_model->getAll();
		$data['product']    = $this->Product_model->getAll();

		$this->Function_model->admin('product', $data);
	}

	public function create()
	{
		$this->Product_model->create();
		$this->session->set_flashdata('notification', 'added');
		redirect('product');
	}

	public function update()
	{
		$this->Product_model->update();
		$this->session->set_flashdata('notification', 'edit');
		redirect('product');
	}

	public function delete($id)
	{
		$this->Product_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('product');
	}
}