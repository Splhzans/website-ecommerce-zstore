<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']      = 'Category';
		$data['categories'] = $this->Category_model->getAll();

		$this->Function_model->admin('category', $data);
	}


	// INSERT

	public function create()
	{
		$this->Category_model->create();
		$this->session->set_flashdata('notification', 'added');
		redirect('category');
	}


	// UPDATE

	public function update()
	{
		$this->Category_model->update();
		$this->session->set_flashdata('notification', 'edit');
		redirect('category');
	}


	// DELETE

	public function delete($id)
	{
		$this->Category_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('category');
	}
}