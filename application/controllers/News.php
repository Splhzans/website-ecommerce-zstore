<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title'] = 'News';
		$data['news']  = $this->News_model->getAll();

		$this->Function_model->user('news', $data);
	}

	public function detail($id)
	{
		$data['title']   = 'News';
		$data['news']    = $this->News_model->get($id);
		$data['product'] = $this->Product_model->getAll();

		$this->Function_model->user('news_detail', $data);
	}
}