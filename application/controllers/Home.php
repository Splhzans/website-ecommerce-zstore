<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title']    = 'Home';
		$data['banner']   = $this->Banner_model->getAll();
		$data['product']  = $this->Product_model->getAll();
		$data['news']     = $this->News_model->getAll();

		$this->Function_model->user('home', $data);
	}
}