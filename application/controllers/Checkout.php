<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$id = $this->uri->segment(2);

		if( !$id) {
			$data['title']   = 'Features';
			$data['user']    = $this->User_model->get();
			$data['address'] = $this->Address_model->getAll();
		} else {
			$data['title']   = 'Checkout';
			$data['user']    = $this->User_model->get();
			$data['address'] = $this->Address_model->getAll();
			$data['jne']     = $this->Rajaongkir_model->cost($id['city'], 'jne');
			$data['tiki']    = $this->Rajaongkir_model->cost($id['city'], 'tiki');
			$data['pos']     = $this->Rajaongkir_model->cost($id['city'], 'pos');
		}

		$this->Function_model->user('checkout', $data);  
		
	}
}