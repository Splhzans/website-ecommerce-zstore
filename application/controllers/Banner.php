<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']  = 'Banner';
		$data['banner'] = $this->Banner_model->getAll();

		$this->Function_model->admin('banner', $data);
	}

	public function create()
	{
		$this->Banner_model->create();
		$this->session->set_flashdata('notification', 'added');
		redirect('banner');
	}

	public function update()
	{
		$this->Banner_model->update();
		$this->session->set_flashdata('notification', 'edit');
		redirect('banner');
	}

	public function delete($id)
	{
		$this->Banner_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('banner');
	}
}