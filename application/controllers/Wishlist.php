<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title']   = 'Wishlist';
		$data['wishlist'] = $this->Wishlist_model->getAll();
		$data['category'] = $this->Category_model->getAll();

		$this->Function_model->user('wishlist', $data);
	}


	// INSERT

	public function create($user, $product)
	{
		$this->Wishlist_model->create($user, $product);
		echo "<script> history.go(-1); </script>";
	}


	// DELETE

	public function delete($user_id, $prod_id)
	{
		$this->Wishlist_model->delete($user_id, $prod_id);
		echo "<script> history.go(-1); </script>";
	}
}