<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title']   = 'Inbox';
		$data['message'] = $this->Message_model->getAll();

		$this->Function_model->admin('inbox', $data);
	}

	public function delete($id)
	{
		$this->Message_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('inbox');
	}
}