<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abouts extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title'] = 'About';

		$this->Function_model->admin('about', $data);
	}
}