<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$key = $this->input->post('keyword', true);

		if( !$key ) { $data['product']  = $this->Product_model->getAll(); }
		else { $data['product']  = $this->Product_model->getManyWhere($key); }

		$data['title']    = 'Shop';
		$data['category'] = $this->Category_model->getAll();
		
		$this->Function_model->user('shop', $data);
	}

	public function category($cat)
	{
		$data['title']    = 'Shop';
		$data['category'] = $this->Category_model->getAll();
		$data['product']  = $this->Product_model->getCategory($cat);
		
		$this->Function_model->user('shop', $data);
	}
	
	public function detail($id)
	{
		$data['title']   = 'Shop';
		$data['prod']    = $this->Product_model->get($id);
		$data['product'] = $this->Product_model->getAll();

		$this->Function_model->user('product_detail', $data);
	}
}