<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 2 ) { redirect('auth/blocked'); }
	}

	public function index()
	{
		$data['title'] = 'User';
		$data['users'] = $this->User_model->getAll();

		$this->Function_model->admin('user', $data);
	}

	public function update()
	{
		$this->User_model->update();
		$this->session->set_flashdata('notification', 'edit');
		redirect('user');
	}

	public function delete($id)
	{
		$this->User_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('user');
	}
}