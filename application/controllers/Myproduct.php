<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myproduct extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( !$this->session->userdata('id') ) { redirect('auth/login'); }
		else if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}

	public function index()
	{
		$data['title']    = 'My Product';
		$data['product']  = $this->Product_model->getUser();
		$data['category'] = $this->Category_model->getAll();

		$this->Function_model->user('myproduct', $data);
	}

	public function detail($id)
	{
		$data['title']   = 'My Product';
		$data['prod']    = $this->Product_model->get($id);
		$data['product'] = $this->Product_model->getAll();
		$data['category'] = $this->Category_model->getAll();

		$this->Function_model->user('myproduct_detail', $data);
	}

	public function create()
	{
		$this->Product_model->create();
		$this->session->set_flashdata('notification', 'added');
		redirect('myproduct');
	}

	public function update()
	{
		$this->Product_model->update();
		$this->session->set_flashdata('notification', 'update');
		redirect('myproduct');
	}

	public function delete($id)
	{
		$this->Product_model->delete($id);
		$this->session->set_flashdata('notification', 'delete');
		redirect('myproduct');
	}
}