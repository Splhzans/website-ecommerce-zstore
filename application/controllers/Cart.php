<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('role_id') == 1 ) { redirect('dashboard'); }
	}
	
	public function create()
	{
		$this->Cart_model->create();
		echo "<script> history.go(-1); </script>";
	}

	public function update()
	{
		$this->Cart_model->update();
		redirect('features');
	}
	
	public function updateLog()
	{
		$this->Cart_model->updateLog();
		redirect('features');
	}

	public function delete($rowid)
	{
		$this->Cart_model->delete($rowid);
		echo "<script> history.go(-1); </script>";
	}

	public function deleteLog($user_id, $prod_id)
	{
		$this->Cart_model->deleteLog($user_id, $prod_id);
		echo "<script> history.go(-1); </script>";
	}
}